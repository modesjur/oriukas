﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollFix : MonoBehaviour
{
    public GameObject MainScrollView, DataScrollView, HourSrollView, ScrollViewHorizontalHoursCounterView;
    private ScrollRect MainRect, DataRect, HourRect;
    private Scrollbar MainScroll, DataScroll, HourScroll;
    
    public Scrollbar VerticalCounterScroll, HorizontalHoursCounterScroll;

    private bool firstLap = true;
    private float ScrollY, ScrollX, ScrollZ;
    public GameObject Content; // for HourseView Y transition to follow;

    void Start()
    {
        MainRect = MainScrollView.GetComponent<ScrollRect>();
        DataRect = DataScrollView.GetComponent<ScrollRect>();
        HourRect = HourSrollView.GetComponent<ScrollRect>();

        MainScroll = MainRect.transform.Find("Scrollbar Vertical").GetComponent<Scrollbar>();
        DataScroll = DataRect.transform.Find("Scrollbar Vertical").GetComponent<Scrollbar>();
        HourScroll = HourRect.transform.Find("Scrollbar Horizontal").GetComponent<Scrollbar>();

        MainRect.enabled = true;
        DataRect.enabled = true;
        MainScroll.value = 1;
        DataScroll.value = 0.5f;
        firstLap = true;

        ScrollY = ScrollViewHorizontalHoursCounterView.GetComponent<RectTransform>().position.y;
        ScrollX = ScrollViewHorizontalHoursCounterView.GetComponent<RectTransform>().position.x;
        ScrollZ = ScrollViewHorizontalHoursCounterView.GetComponent<RectTransform>().position.z;
    }

    void Update()
    {
        if (firstLap) //kad pirma frame praskipintu
        {
            firstLap = false;
        }
        else
        {
            float newY = Content.GetComponent<RectTransform>().localPosition.y + ScrollY;
            Vector3 newPos = new Vector3(ScrollX, newY, ScrollZ);

            RectTransform ScrollRect = ScrollViewHorizontalHoursCounterView.GetComponent<RectTransform>();
            ScrollRect.position = newPos;

            HourScroll.value = HorizontalHoursCounterScroll.value;

            if (VerticalCounterScroll.value > 0.5)
            {
                MainRect.enabled = true;
                float proc = ((VerticalCounterScroll.value - 0.5f) / 0.005f) / 100.00f;
                MainScroll.value = VerticalCounterScroll.value * proc;
                DataRect.StopMovement();
                DataRect.enabled = false;
            }
            else
            {

                DataRect.enabled = true;
                DataScroll.value = VerticalCounterScroll.value;
                MainRect.StopMovement();
                MainRect.enabled = false;

            }
        }
    }
}
