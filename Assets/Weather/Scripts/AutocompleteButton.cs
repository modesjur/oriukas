﻿using UnityEngine;
using UnityEngine.UI;

public class AutocompleteButton : MonoBehaviour
{
    public GameObject CityContent;
    InputField input;
    public int numberInList;
    public string code;

    public void OnPressed()
    {
        input.text = this.GetComponentInChildren<Text>().text;
        foreach (Transform child in CityContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Searching();
        GameObject.Find("PlacePanel").GetComponent<API>().SearchMeteo();
        GameObject.Find("PlacePanel").GetComponent<PlaceScript>().RefreshDataFromApi();
        GameObject.Find("PlacePanel").GetComponent<PlaceScript>().CloseSearch();
        GameObject.Find("PlacePanel").GetComponent<API>().clearAutocomplete();
        input.transform.Find("Text").GetComponent<Text>().text = "";
        //GameObject.Find("PlacePanel").GetComponent<API>().AutofillShow.SetActive(false);
    }
    void Start()
    {
        CityContent = transform.parent.gameObject;
        input = GameObject.Find("InputField").GetComponent<InputField>();
    }
}
