﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeekDayScript : MonoBehaviour
{
    public Text DayNameUI, DayTempUI, NightTempUI;
    public Image ConditionImg;

    public DateTime DayDate;
    public string DayDateString;
    public string DayName="";
    public List<float> DayTemp;
    public List<float> NightTemp;
    public List<string> Conditions; //icons will be like day
    public float MaxDayTemperature, MinNightTemperature;
    public string conditionCode;

    public float maxValueInList(List<float> list)
    {
        float max = -999;
        foreach(float t in list)
        {
            if (t > max) max = t;
        }
        return max;
    }

    public float minValueInList(List<float> list)
    {
        float min = 999;
        foreach (float t in list)
        {
            if (t < min) min = t;
        }
        return min;
    }

    public string MostCommonStringInList(List<string> list)
    {
        string Most = "";
        int MostInt = 0;
        Dictionary<string, int> shoppingDictionary = new Dictionary<string, int>();
        foreach (string item in list)
        {
            if (!shoppingDictionary.ContainsKey(item))
            {
                shoppingDictionary.Add(item, 1);
            }
            else
            {
                int count = 0;
                shoppingDictionary.TryGetValue(item, out count);
                shoppingDictionary.Remove(item);
                shoppingDictionary.Add(item, count + 1);
            }
        }

        foreach (KeyValuePair<string, int> entry in shoppingDictionary)
        {
            if(MostInt < entry.Value)
            {
                MostInt = entry.Value;
                Most = entry.Key;
            }
        }

        return Most;
    }
    public void AddConditionImg()
    {
        if (conditionCode.Equals("clear")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[0];
        else if (conditionCode.Equals("isolated-clouds")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[2];
        else if (conditionCode.Equals("scattered-clouds")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[4];
        else if (conditionCode.Equals("overcast")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[6];
        else if (conditionCode.Equals("light-rain")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[7];
        else if (conditionCode.Equals("moderate-rain")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[9];
        else if (conditionCode.Equals("heavy-rain")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[10];
        else if (conditionCode.Equals("sleet")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[11];
        else if (conditionCode.Equals("light-snow")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[12];
        else if (conditionCode.Equals("moderate-snow")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[13];
        else if (conditionCode.Equals("heavy-snow")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[14];
        else if (conditionCode.Equals("fog")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[15];
        else if (conditionCode.Equals("na")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[16];
        //else Debug.Log("NO DATA FOR HOURS IMG");
    }

    public void AllCalculations()
    {
        MaxDayTemperature = maxValueInList(DayTemp);
        MinNightTemperature = minValueInList(NightTemp);
        conditionCode = MostCommonStringInList(Conditions);
        DayDateString = DayDate.ToString();
    }

    public void UpdateUIData()
    {
        AllCalculations();

        DayNameUI.text = DayName;

        if (UserSettings.Instance.GetC()) //if °C
        {
            if (UserSettings.Instance.GetRoundTemp()) //Round kol kas nuo .5 
            {
                DayTempUI.text = Mathf.RoundToInt(MaxDayTemperature).ToString() + "°";
                NightTempUI.text = Mathf.RoundToInt(MinNightTemperature).ToString() + "°";
            }
            else
            {
                DayTempUI.text = ((Mathf.Round(MaxDayTemperature * 10)) / 10.0f).ToString() + "°";
                NightTempUI.text = ((Mathf.Round(MinNightTemperature * 10)) / 10.0f).ToString() + "°";
            }
        }
        else //if °F
        {
            float Dayf = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().CelsiusToFahrenheit(MaxDayTemperature);
            float Nightf = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().CelsiusToFahrenheit(MinNightTemperature);
            if (UserSettings.Instance.GetRoundTemp()) //Round kol kas nuo .5 
            {
                DayTempUI.text = Mathf.RoundToInt(Dayf).ToString() + "°";
                NightTempUI.text = Mathf.RoundToInt(Nightf).ToString() + "°";
            }
            else
            {
                DayTempUI.text = ((Mathf.Round(Dayf * 10)) / 10.0f).ToString() + "°";
                NightTempUI.text = ((Mathf.Round(Nightf * 10)) / 10.0f).ToString() + "°";
            }
        }

        AddConditionImg();
    }
    void Start()
    {

    }

    void Update()
    {

    }
}
