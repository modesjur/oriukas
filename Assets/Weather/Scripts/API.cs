﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using SimpleJSON;
using System.Net;
using UnityEngine.Networking;

public class API : MonoBehaviour
{
    private string meteo_places = "https://api.meteo.lt/v1/places/";
    private string getLocation = "http://www.geoplugin.net/json.gp?ip=";
    public InputField searchField;
    public GameObject CityContent; //autocomplete list container;
    public GameObject CityPrefab; 
    public GameObject PlacePanel;
    public GameObject AutofillShow;

    public Button Search;

    List<string[]> AllLocations = new List<string[]>();
    void Start()
    {

        AllLocations = getAllCities();
    }

    List<string[]> getAllCities()  //0 - loc name, 1 - loc code, 2 - loc number in alphabetic list
    {
        List<string[]> locations = new List<string[]>();

        string getLocationList = new WebClient().DownloadString(meteo_places);
        getLocationList = "{places:" + getLocationList + "}";
        JSONNode jsonNode = SimpleJSON.JSON.Parse(getLocationList);

        int index = 0;
        foreach (JSONNode o in jsonNode["places"].Children)
        {
            string[] location = new string[] { "", "", ""};

            location[0] = o["name"].ToString().Replace("\"", ""); // nuimti kabutes

            location[1] = o["code"].ToString().Replace("\"", ""); // nuimti kabutes

            location[2] = index.ToString();

            locations.Add(location);
            index++;
        }
        return locations;
    }
    List<string[]> Autocomplete(string typedCharacters)
    {
        List<string[]> possibleMatches = new List<string[]>();

        int index = 0;

        //Fixing Typed data
        typedCharacters = typedCharacters.ToLower();

        typedCharacters = typedCharacters.Replace("\"","").Replace(" ","").Replace("_", "").Replace("(", "").Replace(")", "").Replace("-", "")
            .Replace(";", "").Replace(":", "").Replace("\n", "");
        //LT raides
        typedCharacters = typedCharacters.Replace("ą", "a").Replace("č", "c").Replace("ę", "e").Replace("ė", "e")
            .Replace("į", "i").Replace("š", "s").Replace("ų", "u").Replace("ū", "u").Replace("ž", "z");
        //LV raides
        typedCharacters = typedCharacters.Replace("ā", "a").Replace("ē", "e").Replace("ģ", "g")
        .Replace("ī", "i").Replace("ķ", "k").Replace("ļ", "l").Replace("ņ", "n");
            //BY- RU raides
        typedCharacters = typedCharacters.Replace("б", "b").Replace("в", "v").Replace("г", "g").Replace("д", "d")
        .Replace("ё", "o").Replace("ж", "s").Replace("з", "z").Replace("й", "y").Replace("к", "k").Replace("л", "l")
        .Replace("м", "m").Replace("н", "n").Replace("п", "p").Replace("т", "t").Replace("ў", "w").Replace("ф", "f")
        .Replace("ц", "c").Replace("ч", "c").Replace("ш", "s").Replace("ы", "y").Replace("ь", "").Replace("э", "e")
        .Replace("ю", "ju").Replace("я", "ja");
        //PL raides
        typedCharacters = typedCharacters.Replace("ł", "w").Replace("ń", "n").Replace("ó", "u").Replace("ś", "s")
        .Replace("ź", "z").Replace("ż", "z");
            //EE raides
        typedCharacters = typedCharacters.Replace("ä", "a").Replace("ö", "o").Replace("õ", "o").Replace("ü", "u");

        foreach (string[] curCity in AllLocations)
        {
            //skliausteliuose
            if (curCity[1].Contains("("))
            {
                string tempCityCode = curCity[1].Substring(curCity[0].IndexOf('-') + 1); //istrint kas yra iki - ir -
                if (tempCityCode.StartsWith(typedCharacters)) //temp miesto koda patikrina jei jis sudurtinis
                {
                    possibleMatches.Add(curCity);
                }
            }
            else if (curCity[1].StartsWith(typedCharacters))
            {
                possibleMatches.Add(curCity);
            }
            index++;
        }
        return possibleMatches;
    }

    private void Update()
    {
        if (searchField.text.Equals("") || searchField.text.Equals(" ")) Search.interactable = false;
        else Search.interactable = true;
    }

    string getLocationByUserIp()
    {
        string locName = "";
        string ip = new WebClient().DownloadString("http://icanhazip.com");
        string getIpLocation = new WebClient().DownloadString(getLocation + ip);
        JSONNode jsonNode = SimpleJSON.JSON.Parse(getIpLocation);
        locName = jsonNode["geoplugin_city"].ToString();

        return locName;
    }

    public void clearAutocomplete()
    {
        foreach (Transform child in CityContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        AutofillShow.SetActive(false);
    }

    public void fillAutocomplete()
    {
        
        clearAutocomplete();

        AutofillShow.SetActive(true);

        if (searchField.text != "")
        {
            if (Autocomplete(searchField.text).Count > 0)
            {
                foreach (string[] place in Autocomplete(searchField.text))
                {
                    GameObject _roomListItemGO = Instantiate(CityPrefab);
                    _roomListItemGO.transform.SetParent(CityContent.transform);
                    _roomListItemGO.name = place[0];
                    _roomListItemGO.GetComponentInChildren<Text>().text = place[0];
                    _roomListItemGO.GetComponent<AutocompleteButton>().code = place[1];
                    _roomListItemGO.GetComponent<AutocompleteButton>().numberInList = int.Parse(place[2]);

                    // Debug.Log(s);
                }
            }
            else
            {
                //Vieta nerasta.
                GameObject _roomListItemGO0 = Instantiate(CityPrefab);
                _roomListItemGO0.transform.SetParent(CityContent.transform);
                _roomListItemGO0.name = "Vieta nerasta...";
                _roomListItemGO0.GetComponentInChildren<Text>().text = "Vieta nerasta.";
                _roomListItemGO0.GetComponent<Button>().interactable = false;
            }
        }
        else
        {
            //fill with biggest cities

            //Vilnius
            GameObject _roomListItemGO0 = Instantiate(CityPrefab);
            _roomListItemGO0.transform.SetParent(CityContent.transform);
            _roomListItemGO0.name = "Vilnius";
            _roomListItemGO0.GetComponentInChildren<Text>().text = "Vilnius";
            _roomListItemGO0.GetComponent<AutocompleteButton>().code = "vilnius";
            _roomListItemGO0.GetComponent<AutocompleteButton>().numberInList = int.Parse(2093+"");

            //Kaunas
            GameObject _roomListItemGO1 = Instantiate(CityPrefab);
            _roomListItemGO1.transform.SetParent(CityContent.transform);
            _roomListItemGO1.name = "Kaunas";
            _roomListItemGO1.GetComponentInChildren<Text>().text = "Kaunas";
            _roomListItemGO1.GetComponent<AutocompleteButton>().code = "kaunas";
            _roomListItemGO1.GetComponent<AutocompleteButton>().numberInList = int.Parse(733 + "");

            //Klaipėda
            GameObject _roomListItemGO2 = Instantiate(CityPrefab);
            _roomListItemGO2.transform.SetParent(CityContent.transform);
            _roomListItemGO2.name = "Klaipėda";
            _roomListItemGO2.GetComponentInChildren<Text>().text = "Klaipėda";
            _roomListItemGO2.GetComponent<AutocompleteButton>().code = "klaipeda";
            _roomListItemGO2.GetComponent<AutocompleteButton>().numberInList = int.Parse(795 + "");

            //Palanga
            GameObject _roomListItemGO3 = Instantiate(CityPrefab);
            _roomListItemGO3.transform.SetParent(CityContent.transform);
            _roomListItemGO3.name = "Palanga";
            _roomListItemGO3.GetComponentInChildren<Text>().text = "Palanga";
            _roomListItemGO3.GetComponent<AutocompleteButton>().code = "palanga";
            _roomListItemGO3.GetComponent<AutocompleteButton>().numberInList = int.Parse(1292 + "");

            //Šiauliai
            GameObject _roomListItemGO4 = Instantiate(CityPrefab);
            _roomListItemGO4.transform.SetParent(CityContent.transform);
            _roomListItemGO4.name = "Šiauliai";
            _roomListItemGO4.GetComponentInChildren<Text>().text = "Šiauliai";
            _roomListItemGO4.GetComponent<AutocompleteButton>().code = "siauliai";
            _roomListItemGO4.GetComponent<AutocompleteButton>().numberInList = int.Parse(1664 + "");

            //Panevėžys
            GameObject _roomListItemGO5 = Instantiate(CityPrefab);
            _roomListItemGO5.transform.SetParent(CityContent.transform);
            _roomListItemGO5.name = "Panevėžys";
            _roomListItemGO5.GetComponentInChildren<Text>().text = "Panevėžys";
            _roomListItemGO5.GetComponent<AutocompleteButton>().code = "panevezys";
            _roomListItemGO5.GetComponent<AutocompleteButton>().numberInList = int.Parse(1315 + "");

            //Alytus
            GameObject _roomListItemGO6 = Instantiate(CityPrefab);
            _roomListItemGO6.transform.SetParent(CityContent.transform);
            _roomListItemGO6.name = "Alytus";
            _roomListItemGO6.GetComponentInChildren<Text>().text = "Alytus";
            _roomListItemGO6.GetComponent<AutocompleteButton>().code = "alytus";
            _roomListItemGO6.GetComponent<AutocompleteButton>().numberInList = int.Parse(39 + "");

            //Nida
            GameObject _roomListItemGO7 = Instantiate(CityPrefab);
            _roomListItemGO7.transform.SetParent(CityContent.transform);
            _roomListItemGO7.name = "Neringa (Nida)";
            _roomListItemGO7.GetComponentInChildren<Text>().text = "Neringa (Nida)";
            _roomListItemGO7.GetComponent<AutocompleteButton>().code = "neringa-nida";
            _roomListItemGO7.GetComponent<AutocompleteButton>().numberInList = int.Parse(1191 + "");

            //Marijampolė
            GameObject _roomListItemGO8 = Instantiate(CityPrefab);
            _roomListItemGO8.transform.SetParent(CityContent.transform);
            _roomListItemGO8.name = "Marijampolė";
            _roomListItemGO8.GetComponentInChildren<Text>().text = "Marijampolė";
            _roomListItemGO8.GetComponent<AutocompleteButton>().code = "marijampole";
            _roomListItemGO8.GetComponent<AutocompleteButton>().numberInList = int.Parse(1040 + "");

            //Mažeikiai
            GameObject _roomListItemGO9 = Instantiate(CityPrefab);
            _roomListItemGO9.transform.SetParent(CityContent.transform);
            _roomListItemGO9.name = "Mažeikiai";
            _roomListItemGO9.GetComponentInChildren<Text>().text = "Mažeikiai";
            _roomListItemGO9.GetComponent<AutocompleteButton>().code = "mazeikiai";
            _roomListItemGO9.GetComponent<AutocompleteButton>().numberInList = int.Parse(1052 + "");

            //Jonava
            GameObject _roomListItemGO10 = Instantiate(CityPrefab);
            _roomListItemGO10.transform.SetParent(CityContent.transform);
            _roomListItemGO10.name = "Jonava";
            _roomListItemGO10.GetComponentInChildren<Text>().text = "Jonava";
            _roomListItemGO10.GetComponent<AutocompleteButton>().code = "jonava";
            _roomListItemGO10.GetComponent<AutocompleteButton>().numberInList = int.Parse(612 + "");

            //Utena
            GameObject _roomListItemGO11 = Instantiate(CityPrefab);
            _roomListItemGO11.transform.SetParent(CityContent.transform);
            _roomListItemGO11.name = "Utena";
            _roomListItemGO11.GetComponentInChildren<Text>().text = "Utena";
            _roomListItemGO11.GetComponent<AutocompleteButton>().code = "utena";
            _roomListItemGO11.GetComponent<AutocompleteButton>().numberInList = int.Parse(1943 + "");

            //Kėdainiai
            GameObject _roomListItemGO12 = Instantiate(CityPrefab);
            _roomListItemGO12.transform.SetParent(CityContent.transform);
            _roomListItemGO12.name = "Kėdainiai";
            _roomListItemGO12.GetComponentInChildren<Text>().text = "Kėdainiai";
            _roomListItemGO12.GetComponent<AutocompleteButton>().code = "kedainiai";
            _roomListItemGO12.GetComponent<AutocompleteButton>().numberInList = int.Parse(755 + "");

            //Telšiai
            GameObject _roomListItemGO13 = Instantiate(CityPrefab);
            _roomListItemGO13.transform.SetParent(CityContent.transform);
            _roomListItemGO13.name = "Telšiai";
            _roomListItemGO13.GetComponentInChildren<Text>().text = "Telšiai";
            _roomListItemGO13.GetComponent<AutocompleteButton>().code = "telsiai";
            _roomListItemGO13.GetComponent<AutocompleteButton>().numberInList = int.Parse(1877 + "");


            //AutofillShow.SetActive(false);
            // Debug.Log("search field is empty");
        }
    }

    public void SearchMeteo()
    {
        PlacePanel.SetActive(true);
        //Debug.Log(searchField.text);
        //Debug.Log(searchField.text.ToLower().Replace("(","-").Replace(")","").Replace(" ",""));
        string title = searchField.text.ToLower().Replace("(", "-").Replace(")", "").Replace(" ", "");

        PlacePanel.GetComponent<PlaceScript>().PlaceName = title;
        searchField.text = "";
    }

    public void getHtml(string link) //kad prieit is kitu klasiu jei reiktu
    {
        StartCoroutine(GetHtml(link));
    }
    IEnumerator GetHtml(string link) //get html failiuka
    {
        UnityWebRequest www = UnityWebRequest.Get(link);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            string html = www.downloadHandler.text;
        }
    }
}
