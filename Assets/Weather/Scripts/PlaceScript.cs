﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System;
using System.Net;
using UnityEngine.EventSystems;

public class PlaceScript : MonoBehaviour
{
    private string JsonDataString;
    private string meteo = "https://api.meteo.lt/v1/places/";
    public Dictionary<string, int> CurrentTimeZone = new Dictionary<string, int>();

    private bool NeedRefresh = false;
    public bool FailedToFindPlace = false;

    public GameObject SearchLoadingScreen;
    public Text SearchLoadingText;
    public bool SearchPressed = false;

    public string PlaceName = "-";  //public nes ateina is MAIN scene
    public string PlaceCountryCode;
    public string PlaceCode;
    private JSONNode PlaceJson;
    public Text Time_date;
    public Text PlaceNameUI, PlaceSubNameUI, CurrentTempUI, FeelsLikeUI, FeelsLikeUI2, ChanceOfRainUI, CloudCoverUI, HumidityUI, PressureUI,
                PrecipitationUI, WindDirUIDegree, WindDirUIDirection, WindGustUI, WindSpeedUI;
    public Image WindDirImage;

    public Sprite[] WindDirectionImages;

    public float Now_m;

    public GameObject MainBackgroundPanel, SettingsBackgroundPanel, SearchBackgroundPanel, SearchLoadingBackground;
    public Sprite backgroundNight, backgroundDay;
    public GameObject SearchButton;

    public bool Night;

    public string sunriseToday, sunsetToday;

    public string latitude, longitude;
    public Text sunriseTodayUI, sunsetTodayUI;

    public List<GameObject> DayHourPrefs;
    public GameObject Content_DayHourPrefs;

    public float CurrentTemp, FeelsLikeTemp, CloudCover, Humidity, Pressure, Precipitation, WindDir, WindGust, WindSpeed, ChanceOfRainInt;
    public DateTime forecastCreationTimeUtc, appLastUpdateTimeUtc;

    public bool LoadChecker = false;

    public GameObject SearchPanel;
    public bool SearchPanelShouldClose = false;
    public bool OpenedSearchPanel = false;

    public GameObject WeekDayContent;
    public List<GameObject> WeekDayPrefs;
    public GameObject CurrentWeekDayPref, DebugWeekDayPref6, DebugWeekDayPref7;

    public bool StartLoading = false;
    public Image LoadingRotate;
    public float LoadingCompassSpeed = 20.0f;

    public Scrollbar HoursScrollbar, MainScrollbar, DaysScrollbar, ColtrolVerticleScrollbar, ControlHorisontal, AutocompleteScrollbar, SettingsInfoScrollbar;

    public Sprite[] Conditions;
    /* Conditions ID
        0 clear - giedra diena;
        1 clear - giedra naktis;
        2 isolated-clouds - mažai debesuota diena;
        3 isolated-clouds - mažai debesuota naktis;
        4 scattered-clouds - debesuota su pragiedruliais diena;
        5 scattered-clouds - debesuota su pragiedruliais naktis;       
        6 overcast - debesuota;
        7 light-rain - nedidelis lietus diena;
        8 light-rain - nedidelis lietus naktis;
        9 moderate-rain - lietus;
        10 heavy-rain - smarkus lietus;
        11 sleet - šlapdriba;
        12 light-snow - nedidelis sniegas;
        13 moderate-snow - sniegas;
        14 heavy-snow - smarkus sniegas;
        15 fog - rūkas;
        16 na - oro sąlygos nenustatytos.
    */

    void Start()
    {
        TrilleonAutomation.AutomationMaster.Initialize(); //Testu Programos paleidimas pries viska

        //timezones list https://nodatime.org/TimeZones#notes arba https://www.zeitverschiebung.net/en/timezone/europe--vilnius
        CurrentTimeZone.Add("LT", 3); //+02, +03
        CurrentTimeZone.Add("EE", 3); //+02, +03
        CurrentTimeZone.Add("LV", 3); //+02, +03
        CurrentTimeZone.Add("PL", 2); //+01, +02
        CurrentTimeZone.Add("BY", 3); //+03
        CurrentTimeZone.Add("RU", 2); //+02

        Time_date.text = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        foreach (Transform child in Content_DayHourPrefs.transform)
        {
            DayHourPrefs.Add(child.gameObject);
        }

        WeekDayPrefs.Add(CurrentWeekDayPref);
        foreach (Transform child in WeekDayContent.transform)
        {
            WeekDayPrefs.Add(child.gameObject);
        }
        WeekDayPrefs.Add(DebugWeekDayPref6); //Del bugo api
        WeekDayPrefs.Add(DebugWeekDayPref7);//Del bugo api

        //Default lokacija pradzioj
        PlaceName = "Vilnius";
        FixScrollBarPlacePanel();
        RefreshDataFromApi();
        CloseSearchLoading();
    }

    public bool IsItNight(string HHmm)
    {
        float now = float.Parse(HHmm.Replace(" ", "").Replace(":", "."));
        float sunrise = float.Parse(sunriseToday.Replace(" ", "").Replace(":", "."));
        float sunset = float.Parse(sunsetToday.Replace(" ", "").Replace(":", "."));
        if (now < sunrise || now > sunset) return true;
        else return false;
    }
    public void getSunriseSunset()
    {
        string DateToday = System.DateTime.UtcNow.ToString("yyyy-MM-dd");
        string DateTomorow = System.DateTime.UtcNow.AddDays(1).ToString("yyyy-MM-dd");
        string[] today = getSunriseSunsetfromAPI(DateToday);//now

        sunriseToday = today[0];
        sunsetToday = today[1];

        string HHmm = System.DateTime.UtcNow.AddHours(CurrentTimeZone[PlaceCountryCode]).ToString("HH:mm");
        Night = IsItNight(HHmm);
    }
    string[] getSunriseSunsetfromAPI(string date) 
    {
        
        string link = "https://api.sunrise-sunset.org/json?lat=" + latitude + "&lng=" + longitude + "&date=" + date + "&formatted=0";
        string getJson = new WebClient().DownloadString(link);
        JSONNode jsonNode = SimpleJSON.JSON.Parse(getJson);

        DateTime sunrise = Convert.ToDateTime(jsonNode["results"]["sunrise"].ToString().Replace("\"",""));
        sunrise.AddHours(CurrentTimeZone[PlaceCountryCode]);
        DateTime sunset = Convert.ToDateTime(jsonNode["results"]["sunset"].ToString().Replace("\"", ""));
        sunset.AddHours(CurrentTimeZone[PlaceCountryCode]);
        string[] rise_set = new string[] { sunrise.ToString("HH: mm").Replace(" ",""), sunset.ToString("HH: mm").Replace(" ", "") };

        return rise_set;
    }

    public void FixScrollBarPlacePanel()
    {
        HoursScrollbar.value = 0;
        MainScrollbar.value = 1;
        DaysScrollbar.value = 0.49f;
        ColtrolVerticleScrollbar.value = 1;
        ControlHorisontal.value = 0;
    }
    public void OpenSearchLoading()
    {
        if (SearchLoadingScreen != null)
        {
            SearchLoadingText.text = "ORIUKAS\nKraunasi...";
            Animator animator = SearchLoadingScreen.GetComponent<Animator>();
            if (animator != null)
            {
                animator.SetBool("Open", false);
                StartLoading = true;
            }
        }
    }

    public void CloseSearchLoading()
    {
        if (SearchLoadingScreen != null)
        {
            Animator animator = SearchLoadingScreen.GetComponent<Animator>();
            if (animator != null)
            {
                StartLoading = false;
                LoadingRotate.transform.eulerAngles = Vector3.forward * 0; //atstatytu i pradine busena
                animator.SetBool("Open", true);
            }

        }
    }

    public void Searching()
    {
        OpenSearchLoading();
        SearchPressed = true;
    }

    public void OpenSearch()
    {
        if (SearchPanel != null)
        {
            Animator animator = SearchPanel.GetComponent<Animator>();
            if(animator != null)
            {
                OpenedSearchPanel = true;
                AutocompleteScrollbar.value = 0;
                animator.SetBool("Open", true);
            }
        }
    }

    public void CloseSearch()
    {
        if (SearchPanel != null)
        {
            if (LoadChecker)
            {
                Animator animator = SearchPanel.GetComponent<Animator>();
                if (animator != null)
                {
                    OpenedSearchPanel = false;
                    FixScrollBarPlacePanel();
                    animator.SetBool("Open", false);
                }
            }
            else SearchPanelShouldClose = false;
        }
    }

    public string CleanPlaceName(string name)
    {
        string newName = name;

        //Fixing Typed data
        newName = newName.ToLower();
        //LT raides
        newName = newName.Replace("ą", "a").Replace("č", "c").Replace("ę", "e").Replace("ė", "e")
            .Replace("į", "i").Replace("š", "s").Replace("ų", "u").Replace("ū", "u").Replace("ž", "z");
        //LV raides
        newName = newName.Replace("ā", "a").Replace("ē", "e").Replace("ģ", "g")
        .Replace("ī", "i").Replace("ķ", "k").Replace("ļ", "l").Replace("ņ", "n");
        //BY- RU raides
        newName = newName.Replace("б", "b").Replace("в", "v").Replace("г", "g").Replace("д", "d")
        .Replace("ё", "o").Replace("ж", "s").Replace("з", "z").Replace("й", "y").Replace("к", "k").Replace("л", "l")
        .Replace("м", "m").Replace("н", "n").Replace("п", "p").Replace("т", "t").Replace("ў", "w").Replace("ф", "f")
        .Replace("ц", "c").Replace("ч", "c").Replace("ш", "s").Replace("ы", "y").Replace("ь", "").Replace("э", "e")
        .Replace("ю", "ju").Replace("я", "ja");
        //PL raides
        newName = newName.Replace("ł", "w").Replace("ń", "n").Replace("ó", "u").Replace("ś", "s")
        .Replace("ź", "z").Replace("ż", "z");
        //EE raides
        newName = newName.Replace("ä", "a").Replace("ö", "o").Replace("õ", "o").Replace("ü", "u");

        return newName;
    }

    public void ActivateInputField()
    {
        if (!TouchScreenKeyboard.visible) // debug inputfield ir android keyboard!
        {
            GameObject.Find("InputField").GetComponent<InputField>().Select();
            GameObject.Find("InputField").GetComponent<InputField>().ActivateInputField();
        }
    }
    void Update() //PAGALVOTI APIE FIXED UPDATE KAS MINUTE
    {
        if (StartLoading)
        {
            LoadingRotate.transform.eulerAngles += Vector3.forward * LoadingCompassSpeed;
            //  LoadingRotate.transform.Rotate();
        }

        if(FailedToFindPlace)
        {
            SearchLoadingText.text = "Tokios vietovės nėra...";
            FailedToFindPlace = false;
            SearchPanelShouldClose = false;
            LoadChecker = true;
            PlaceName = ""; // nes kitaip uzkrautu su nesamone po 1m update xd
            StartCoroutine(LoadingCloseigWaitting(2));
            GetComponent<API>().fillAutocomplete();
        }
        if (SearchPanelShouldClose && SearchPressed)
        {
            SearchPressed = false;
            CloseSearch();
            StartCoroutine(LoadingCloseigWaitting(1));
        }

        IEnumerator LoadingCloseigWaitting(int sec)
        {
            yield return new WaitForSeconds(sec);
            CloseSearchLoading();
        }

        if (PlaceName != "" && PlaceName != "-" && PlaceName != "--")
        {
            if (UserSettings.Instance.appLastUpdatedForecastTimeUtc < System.DateTime.UtcNow.AddHours(-1)) //Kas Valanda
            {
                RefreshDataFromApi();
                UserSettings.Instance.appLastUpdatedForecastTimeUtc = System.DateTime.UtcNow;
                //Debug.Log("VALANDA: dabar yra" + System.DateTime.UtcNow);
            }
        }

        //NeedRefresh = true every 1 hour (del RefreshDataFromApi())
        if (NeedRefresh)
        {
            ExtractData();
            getSunriseSunset();
            UpdateUI();
            NeedRefresh = false;
        }

        if (PlaceName != "" && PlaceName != "-" && PlaceName != "--")
        {
            if (UserSettings.Instance.appLastMinuteUpdateTimeUtc < System.DateTime.UtcNow.AddMinutes(-1)) //Kas minute
            {
                if (UserSettings.Instance.GetSmoothHourlyTransition())
                {
                    CalcSmoothHourlyTransition();
                    UpdateUI();
                }
                UserSettings.Instance.appLastMinuteUpdateTimeUtc = System.DateTime.UtcNow;
                //Debug.Log("MINUTE: dabar yra" + System.DateTime.UtcNow);
            }
        }
    }

    public string DayOfTheWeekName(int day)
    {
        int dayID = (day + 6) % 7 + 1;
        string DayName="";
        if (dayID == 1) DayName = "Pirmadienis";
        else if (dayID == 2) DayName = "Antradienis";
        else if (dayID == 3) DayName = "Trečiadienis";
        else if (dayID == 4) DayName = "Ketvirtadienis";
        else if (dayID == 5) DayName = "Penktadienis";
        else if (dayID == 6) DayName = "Šeštadienis";
        else if (dayID == 7) DayName = "Sekmadienis";
        else DayName = "";
        return DayName;
    }

    public void RefreshDataFromApi()
    {
        LoadChecker = false;
        appLastUpdateTimeUtc = System.DateTime.UtcNow;
        StartCoroutine(Meteo(CleanPlaceName(PlaceName)));
    }

    public void CalcSmoothHourlyTransition()
    {
        float temp0, temp1;
        Now_m = int.Parse(System.DateTime.UtcNow.ToString("mm"));
        if (Now_m < 0) Now_m = 0;
        else if (Now_m > 60) Now_m = 60;

        //Current Temp
        temp0 = DayHourPrefs[0].GetComponent<DayHourPrefScript>().airTemperature;
        temp1 = DayHourPrefs[1].GetComponent<DayHourPrefScript>().airTemperature;
        CurrentTemp = temp0 - ((temp0 - temp1) * Now_m / 60);

        //Feels like
        temp0 = DayHourPrefs[0].GetComponent<DayHourPrefScript>().realFeel;
        temp1 = DayHourPrefs[1].GetComponent<DayHourPrefScript>().realFeel;
        FeelsLikeTemp = temp0 - ((temp0 - temp1) * Now_m / 60);

        //Cloud Cover
        temp0 = DayHourPrefs[0].GetComponent<DayHourPrefScript>().cloudCover;
        temp1 = DayHourPrefs[1].GetComponent<DayHourPrefScript>().cloudCover;
        CloudCover = temp0 - ((temp0 - temp1) * Now_m / 60);

        //Humidity
        temp0 = DayHourPrefs[0].GetComponent<DayHourPrefScript>().relativeHumidity;
        temp1 = DayHourPrefs[1].GetComponent<DayHourPrefScript>().relativeHumidity;
        Humidity = temp0 - ((temp0 - temp1) * Now_m / 60);

        //Pressure
        temp0 = DayHourPrefs[0].GetComponent<DayHourPrefScript>().seaLevelPressure;
        temp1 = DayHourPrefs[1].GetComponent<DayHourPrefScript>().seaLevelPressure;
        Pressure = temp0 - ((temp0 - temp1) * Now_m / 60);

        //Precipitation
        temp0 = DayHourPrefs[0].GetComponent<DayHourPrefScript>().totalPrecipitation;
        temp1 = DayHourPrefs[1].GetComponent<DayHourPrefScript>().totalPrecipitation;
        Precipitation = temp0 - ((temp0 - temp1) * Now_m / 60);

        //WindDir
        temp0 = DayHourPrefs[0].GetComponent<DayHourPrefScript>().windDirection;
        temp1 = DayHourPrefs[1].GetComponent<DayHourPrefScript>().windDirection;
        WindDir = temp0 - ((temp0 - temp1) * Now_m / 60);

        // WindGust
        temp0 = DayHourPrefs[0].GetComponent<DayHourPrefScript>().windGust;
        temp1 = DayHourPrefs[1].GetComponent<DayHourPrefScript>().windGust;
        WindGust = temp0 - ((temp0 - temp1) * Now_m / 60);

        // WindSpeed
        temp0 = DayHourPrefs[0].GetComponent<DayHourPrefScript>().windSpeed;
        temp1 = DayHourPrefs[1].GetComponent<DayHourPrefScript>().windSpeed;
        WindSpeed = temp0 - ((temp0 - temp1) * Now_m / 60);

        // WindSpeed
        temp0 = DayHourPrefs[0].GetComponent<DayHourPrefScript>().ChanceOfRainInt;
        temp1 = DayHourPrefs[1].GetComponent<DayHourPrefScript>().ChanceOfRainInt;
        ChanceOfRainInt = temp0 - ((temp0 - temp1) * Now_m / 60);

    }
    string UppercaseFirst(string str)
    {
        if (string.IsNullOrEmpty(str))
            return string.Empty;
        return char.ToUpper(str[0]) + str.Substring(1).ToLower();
    }
    public void UpdateUI()
    {
        //background
        Image img1 = MainBackgroundPanel.GetComponent<Image>();
        Image img2 = SettingsBackgroundPanel.GetComponent<Image>();
        Image img3 = SearchBackgroundPanel.GetComponent<Image>();
        Image img4 = SearchLoadingBackground.GetComponent<Image>();

        if (Night)
        {
            img1.sprite = backgroundNight;
            img2.sprite = backgroundNight;
            img3.sprite = backgroundNight;
            img4.sprite = backgroundNight;
            Color Dark = new Color(24 / 255.0f, 37 / 255.0f, 45 / 255.0f, 255 / 255.0f);
            SearchButton.GetComponent<Image>().color = Dark;
        }
        else
        {
            img1.sprite = backgroundDay;
            img2.sprite = backgroundDay;
            img3.sprite = backgroundDay;
            img4.sprite = backgroundDay;
            Color Blue = new Color(1 / 255.0f, 106 / 255.0f, 197 / 255.0f, 255 / 255.0f);
            SearchButton.GetComponent<Image>().color = Blue;
        }

        //current panel

        if(PlaceName.Contains("("))
        {
            string[] x = PlaceName.Split('(');
            x[0] = UppercaseFirst(x[0].Replace("\"", "").Replace("(", "").Replace(")", ""));
            x[1] = UppercaseFirst(x[1].Replace("\"", "").Replace("(", "").Replace(")", ""));
            PlaceNameUI.text = x[0];
            PlaceSubNameUI.text = x[1];
        }
        else
        {
            PlaceNameUI.text = UppercaseFirst(PlaceName.Replace("\"", ""));
            PlaceSubNameUI.text = "Centras";
        }



        //24h panel
        foreach (GameObject Pref in DayHourPrefs) Pref.GetComponent<DayHourPrefScript>().UpdateUIData();

        //WeekDays panel
        foreach (GameObject Pref in WeekDayPrefs) Pref.GetComponent<WeekDayScript>().UpdateUIData();

        //current panel

        if (UserSettings.Instance.GetC()) //if °C
        {
            if (UserSettings.Instance.GetRoundTemp()) //Round kol kas nuo .5 
            {
                CurrentTempUI.text = Mathf.RoundToInt(CurrentTemp).ToString() + "°";
                FeelsLikeUI.text = "Juntamoji " + Mathf.RoundToInt(FeelsLikeTemp).ToString() + "°";
                FeelsLikeUI2.text = Mathf.RoundToInt(FeelsLikeTemp).ToString() + "°";
            }
            else
            {
                CurrentTempUI.text = ((Mathf.Round(CurrentTemp * 10)) / 10.0f).ToString() + "°";
                FeelsLikeUI.text = "Juntamoji " + ((Mathf.Round(FeelsLikeTemp * 10)) / 10.0f).ToString() + "°";
                FeelsLikeUI2.text = ((Mathf.Round(FeelsLikeTemp * 10)) / 10.0f).ToString() + "°";
            }
        }
        else //if °F 
        {
            float Tempf = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().CelsiusToFahrenheit(CurrentTemp);
            float Feelf = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().CelsiusToFahrenheit(FeelsLikeTemp);

            if (UserSettings.Instance.GetRoundTemp()) //Round kol kas nuo .5 
            {
                CurrentTempUI.text = " " + Mathf.RoundToInt(Tempf).ToString() + "°";
                FeelsLikeUI.text = "Juntamoji " + Mathf.RoundToInt(Feelf).ToString() + "°";
                FeelsLikeUI2.text = Mathf.RoundToInt(Feelf).ToString() + "°";
            }
            else // nerounded 11.4
            {
                CurrentTempUI.text = ((Mathf.Round(Tempf * 10)) / 10.0f).ToString() + "°";
                FeelsLikeUI.text = "Juntamoji " + ((Mathf.Round(Feelf * 10)) / 10.0f).ToString() + "°";
                FeelsLikeUI2.text = ((Mathf.Round(Feelf * 10)) / 10.0f).ToString() + "°";
            }
        }

        if (UserSettings.Instance.GetRoundTemp()) //Round kol kas nuo .5 
        {
            CloudCoverUI.text = Mathf.RoundToInt(CloudCover).ToString() + " %";
            HumidityUI.text = Mathf.RoundToInt(Humidity).ToString() + " %";
            PressureUI.text = Mathf.RoundToInt(Pressure).ToString() + " hPa";
            PrecipitationUI.text = Mathf.RoundToInt(Precipitation).ToString() + " mm/val";
            WindDirUIDegree.text = Mathf.RoundToInt(WindDir).ToString() + "°";
            WindGustUI.text = Mathf.RoundToInt(WindGust).ToString() + " m/s";
            WindSpeedUI.text = Mathf.RoundToInt(WindSpeed).ToString() + " m/s";
        }
        else
        {
            CloudCoverUI.text = ((Mathf.Round(CloudCover * 10)) / 10.0f).ToString() + " %";
            HumidityUI.text = ((Mathf.Round(Humidity * 10)) / 10.0f).ToString() + " %";
            PressureUI.text = ((Mathf.Round(Pressure * 10)) / 10.0f).ToString() + " mm";
            PrecipitationUI.text = ((Mathf.Round(Precipitation * 10)) / 10.0f).ToString() + " mm/val";
            WindDirUIDegree.text = ((Mathf.Round(WindDir * 10)) / 10.0f).ToString() + "°";
            WindGustUI.text = ((Mathf.Round(WindGust * 10)) / 10.0f).ToString() + " m/s";
            WindSpeedUI.text = ((Mathf.Round(WindSpeed * 10)) / 10.0f).ToString() + " m/s";
        }
        WindDirUIDirection.text = CalcWindDir(WindDir);
        ChangeWindDirImg(CalcWindDir(WindDir));

        //Chance FO Rain
        ChanceOfRainUI.text = ChanceOfRainInt + "%";

        //SunsetSunrise panel
        sunriseTodayUI.text = sunriseToday;
        sunsetTodayUI.text = sunsetToday;

        LoadChecker = true;
        if (OpenedSearchPanel) SearchPanelShouldClose = true;
    }

    public void ChangeWindDirImg(string dir)
    {
        if (dir.Equals("P")) WindDirImage.sprite = WindDirectionImages[0];
        else if (dir.Equals("PV")) WindDirImage.sprite = WindDirectionImages[1];
        else if (dir.Equals("V")) WindDirImage.sprite = WindDirectionImages[2];
        else if (dir.Equals("ŠV")) WindDirImage.sprite = WindDirectionImages[3];
        else if (dir.Equals("Š")) WindDirImage.sprite = WindDirectionImages[4];
        else if (dir.Equals("ŠR")) WindDirImage.sprite = WindDirectionImages[5];
        else if (dir.Equals("R")) WindDirImage.sprite = WindDirectionImages[6];
        else if (dir.Equals("PR")) WindDirImage.sprite = WindDirectionImages[7];
    }
    public string CalcWindDir(float d)
    {
        string dir = "";
        if (d < 22.5f && d >= 0 || d > 337.5f && d <= 360)
            dir = "Š";
        else if (d >= 22.5f && d >= 67.5f)
            dir = "ŠR";
        else if (d > 67.5f && d < 112.5f)
            dir = "R";
        else if (d >= 112.5f && d <= 157.5f)
            dir = "PR";
        else if (d > 157.5f && d < 202.5f)
            dir = "P";
        else if (d >= 202.5f && d <= 247.5f)
            dir = "PV";
        else if (d > 247.5f && d < 292.5f)
            dir = "V";
        else if (d >= 292.5f && d <= 337.5f)
            dir = "ŠV";
        return dir;
    }

    public void ExtractData()
    {
        if (!PlaceJson.ToString().Equals(""))
        {
            DateTime TimeNow = System.DateTime.UtcNow.AddHours(CurrentTimeZone[PlaceCountryCode]);
            DateTime ScannedDate = TimeNow;
            int index = 0;
            int WeekDayIndex = 0;
            foreach (JSONNode o in PlaceJson["temp"].Children)
            {
                string CurrentTempDate = o["forecastTimeUtc"];
                DateTime ForecastTime = Convert.ToDateTime(CurrentTempDate);
                DateTime PlaceCurrentTime = Convert.ToDateTime(CurrentTempDate).AddHours(CurrentTimeZone[PlaceCountryCode]);

                if (index <= 24)  // now + 24h data
                {
                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().id = index;
                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().forecastTimeUtc = ForecastTime;
                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().forecastTimeByTimeZone = PlaceCurrentTime;

                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().airTemperature = Mathf.Round(float.Parse(o["airTemperature"].ToString()) * 100f) / 100f; // 0.00 tikslumu kad butu
                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().cloudCover = float.Parse(o["cloudCover"]) * 100f / 100f; // 0.00 tikslumu kad butu
                    if (o["relativeHumidity"].ToString().Equals("null")) DayHourPrefs[index].GetComponent<DayHourPrefScript>().relativeHumidity = 0;
                    else DayHourPrefs[index].GetComponent<DayHourPrefScript>().relativeHumidity = Mathf.Round(float.Parse(o["relativeHumidity"].ToString()) * 100f) / 100f;
                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().seaLevelPressure = Mathf.Round(float.Parse(o["seaLevelPressure"].ToString()) * 100f) / 100f; // 0.00 tikslumu kad butu
                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().totalPrecipitation = Mathf.Round(float.Parse(o["totalPrecipitation"].ToString()) * 100f) / 100f; // 0.00 tikslumu kad butu
                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().windDirection = Mathf.Round(float.Parse(o["windDirection"].ToString()) * 100f) / 100f; // 0.00 tikslumu kad butu
                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().windGust = Mathf.Round(float.Parse(o["windGust"].ToString()) * 100f) / 100f; // 0.00 tikslumu kad butu
                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().windSpeed = Mathf.Round(float.Parse(o["windSpeed"].ToString()) * 100f) / 100f; // 0.00 tikslumu kad butu
                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().conditionCode = o["conditionCode"].ToString().Replace("\"", ""); // nuimti kabutes

                    DayHourPrefs[index].GetComponent<DayHourPrefScript>().AllCalculations();
                }
                index++;

                if (!sunriseToday.Equals("") && sunriseToday != null)
                {
                    
                    if (PlaceCurrentTime.Day >= TimeNow.Day)
                    {
                        if (PlaceCurrentTime.Day == ScannedDate.Day)
                        {
                            if (WeekDayIndex == 0)
                            {
                                if (CurrentWeekDayPref.GetComponent<WeekDayScript>().DayName.Equals(""))
                                {
                                    string name = DayOfTheWeekName((int)PlaceCurrentTime.DayOfWeek);
                                    CurrentWeekDayPref.GetComponent<WeekDayScript>().DayName = name;
                                    CurrentWeekDayPref.GetComponent<WeekDayScript>().DayDate = PlaceCurrentTime;
                                }
                                if (IsItNight(PlaceCurrentTime.ToString("HH:mm")))
                                    if(PlaceCurrentTime.Hour < 12) //Kad imtu tik nakti tarp 0 - 12h. 12h jau tikrai nebebus naktis
                                        CurrentWeekDayPref.GetComponent<WeekDayScript>().NightTemp.Add(o["airTemperature"]);
                                else
                                    CurrentWeekDayPref.GetComponent<WeekDayScript>().DayTemp.Add(o["airTemperature"]);

                                CurrentWeekDayPref.GetComponent<WeekDayScript>().Conditions.Add(o["conditionCode"].ToString().Replace("\"", ""));
                            }
                            else if (WeekDayIndex >0)
                            {
                                try
                                {
                                    if (WeekDayContent.transform.Find("Day (" + WeekDayIndex + ")").GetComponent<WeekDayScript>().DayName.Equals(""))
                                    {
                                        string name = DayOfTheWeekName((int)PlaceCurrentTime.DayOfWeek);
                                        WeekDayContent.transform.Find("Day (" + WeekDayIndex + ")").GetComponent<WeekDayScript>().DayName = name;
                                        WeekDayContent.transform.Find("Day (" + WeekDayIndex + ")").GetComponent<WeekDayScript>().DayDate = PlaceCurrentTime;
                                    }

                                    if (IsItNight(PlaceCurrentTime.ToString("HH:mm")))
                                        if (PlaceCurrentTime.Hour < 12) //Kad imtu tik nakti tarp 0 - 12h. 12h jau tikrai nebebus naktis
                                            WeekDayContent.transform.Find("Day (" + WeekDayIndex + ")").GetComponent<WeekDayScript>().NightTemp.Add(o["airTemperature"]);
                                        else
                                            WeekDayContent.transform.Find("Day (" + WeekDayIndex + ")").GetComponent<WeekDayScript>().DayTemp.Add(o["airTemperature"]);

                                    WeekDayContent.transform.Find("Day (" + WeekDayIndex + ")").GetComponent<WeekDayScript>().Conditions.Add(o["conditionCode"].ToString().Replace("\"", ""));

                                }
                                catch (Exception e)
                                {
                                    if (WeekDayIndex < 6) Debug.Log("ERORAS");
                                    else
                                    {
                                        /*
                                        Debug.Log(WeekDayIndex);
                                        WeekDayContent.transform.Find("Day (" + WeekDayIndex + ")").GetComponent<WeekDayScript>().DayName = "Bugged";
                                        WeekDayContent.transform.Find("Day (" + WeekDayIndex + ")").GetComponent<WeekDayScript>().DayDate = System.DateTime.Now;
                                        WeekDayContent.transform.Find("Day (" + WeekDayIndex + ")").GetComponent<WeekDayScript>().NightTemp.Add(999);
                                        WeekDayContent.transform.Find("Day (" + WeekDayIndex + ")").GetComponent<WeekDayScript>().DayTemp.Add(999);
                                        WeekDayContent.transform.Find("Day (" + WeekDayIndex + ")").GetComponent<WeekDayScript>().Conditions.Add("clear".ToString().Replace("\"", ""));*/
                                    }
                                }
                            }
                        }
                        else
                        {
                            WeekDayIndex++;
                            ScannedDate = PlaceCurrentTime;
                        }
                    }
                }
            }
            //current temp
            CurrentTemp = DayHourPrefs[0].GetComponent<DayHourPrefScript>().airTemperature;
            FeelsLikeTemp = DayHourPrefs[0].GetComponent<DayHourPrefScript>().realFeel;
            ChanceOfRainInt = DayHourPrefs[0].GetComponent<DayHourPrefScript>().ChanceOfRainInt;
            CloudCover = DayHourPrefs[0].GetComponent<DayHourPrefScript>().cloudCover;
            Humidity = DayHourPrefs[0].GetComponent<DayHourPrefScript>().relativeHumidity;
            Pressure = DayHourPrefs[0].GetComponent<DayHourPrefScript>().seaLevelPressure;
            Precipitation = DayHourPrefs[0].GetComponent<DayHourPrefScript>().totalPrecipitation;
            WindDir = DayHourPrefs[0].GetComponent<DayHourPrefScript>().windDirection;
            WindGust = DayHourPrefs[0].GetComponent<DayHourPrefScript>().windGust;
            WindSpeed = DayHourPrefs[0].GetComponent<DayHourPrefScript>().windSpeed;

            if (UserSettings.Instance.GetSmoothHourlyTransition())
            {
                CalcSmoothHourlyTransition();
            }
            UpdateUI();
        }
        else
        {
            Debug.Log("(ExtractData) JSON not found");
        }
    }

    IEnumerator Meteo(string title)
    {
        JsonDataString = "";
        string apiRequest = meteo + title + "/forecasts/long-term";
        WWW readingsite = new WWW(apiRequest);
        yield return readingsite;

        if (string.IsNullOrEmpty(readingsite.error))
        {
            JsonDataString = readingsite.text;
        }
        JSONNode jsonNode = SimpleJSON.JSON.Parse(JsonDataString);
        try
        {
            PlaceName = jsonNode["place"]["name"].ToString();
            PlaceCountryCode = jsonNode["place"]["countryCode"].ToString().Replace("\"", ""); // nuimti kabutes;

            latitude = jsonNode["place"]["coordinates"]["latitude"].ToString();
            longitude = jsonNode["place"]["coordinates"]["longitude"].ToString();

            string TempforecastCreationTimeUtc = jsonNode["forecastCreationTimeUtc"];
            forecastCreationTimeUtc = Convert.ToDateTime(TempforecastCreationTimeUtc);

            string forecastTimestamps = jsonNode["forecastTimestamps"].ToString();
            forecastTimestamps = "{temp:" + forecastTimestamps + "}";
            PlaceJson = SimpleJSON.JSON.Parse(forecastTimestamps);

            NeedRefresh = true;
        }
        catch(Exception e)
        {
            FailedToFindPlace = true;
        }
        // Debug.Log(JsonDataString);
    }

    public float FahrenheitToCelsius(float F)
    {
        return (F - 32) * 5 / 9;
    }
    public float CelsiusToFahrenheit(float C)
    {
        return (C * 9 / 5) + 32;
    }
    public float MPHtoKMH(float MPH)
    {
        return MPH * 1.609344f;
    }
    public float KMHtoMPH(float KMH)
    {
        return KMH * 0.6213711922f;
    }
    public float KMHtoMS(float KMH)
    {
        return KMH/3.6f;
    }
    public float MStoKMH(float MS)
    {
        return MS * 3.6f;
    }

    public float CalcRealFeel(float Temp_C, float Humidity_Proc, float WindSpeed_MS) // temp C, Humidity % and WindSpeed MS
    {
        Temp_C = CelsiusToFahrenheit(Temp_C); // is C i F konvertuojam
        WindSpeed_MS = KMHtoMPH(MStoKMH(WindSpeed_MS)); // is MS i KMH i MPH
        float RealFeel;

        //first WindChill
        if(Temp_C<=50 && WindSpeed_MS >=3)
        {
            RealFeel = 35.74f + (0.6215f * Temp_C) - 35.75f * Mathf.Pow(WindSpeed_MS, 0.16f) + ((0.4275f * Temp_C) * Mathf.Pow(WindSpeed_MS, 0.16f));

        }
        else RealFeel = Temp_C;

        //Tikrint su Heat Index        
        if (RealFeel == Temp_C)
        {
            RealFeel = 0.5f * (Temp_C + 61.0f + ((Temp_C - 68.0f) * 1.2f) + (Humidity_Proc * 0.094f));

            if (RealFeel >= 80)
            {
                RealFeel = -42.379f + 2.04901523f * Temp_C + 10.14333127f * Humidity_Proc - .22475541f * Temp_C * Humidity_Proc - .00683783f * Temp_C * Temp_C
                    - .05481717f * Humidity_Proc * Humidity_Proc + .00122874f * Temp_C * Temp_C * Humidity_Proc + .00085282f * Temp_C * Humidity_Proc * Humidity_Proc
                    - .00000199f * Temp_C * Temp_C * Humidity_Proc * Humidity_Proc;

                if (Humidity_Proc < 13 && Temp_C >= 80 && Temp_C <= 112)
                {
                    RealFeel = RealFeel - ((13 - Humidity_Proc) / 4) * Mathf.Sqrt((17 - Mathf.Abs(Temp_C - 95.0f)) / 17);

                    if (Humidity_Proc > 85 && Temp_C >= 80 && Temp_C <= 87)
                    {
                        RealFeel = RealFeel + ((Humidity_Proc - 85) / 10) * ((87 - Temp_C) / 5);
                    }
                }

            }
        }

        RealFeel = FahrenheitToCelsius(RealFeel); // F to C
        return RealFeel;
    }
}


//INFORMATION

// 1 mm = 0.0393701 inch
// 1 inch = 25.4 mm

/*
Forecast Terminology	PoP	Precipitation's Areal Coverage
--	Less than 20%	Drizzle, sprinkle (flurries)
Slight chance	20%-40%	Isolated
Chance	50-80%	Scattered
Likely	80-100%	Numerous, Overcast
*/

/*
Terminology	Rainfall Rate
Very light	< 0.01 inch per hour            
Light	0.01 to 0.1 inch per hour               0.1-0.3m
Moderate	0.1 to 0.3 inches per hour          0.6mm
Heavy	>0.3 inches per hour                
 */

/*
 * http://wxbrad.com/why-a-50-chance-of-rain-usually-means-a-100-chance-of-confusion/
 * https://sciencenotes.org/percent-chance-rain-mean/
 
 Chance of rain = Confidence * Areal coverage

1)  Confidence = How confident they are that precipitation will fall somewhere within the forecast area.
2)  Areal coverage = How much of the area will get measurable (at least 0.01 inch) rain or snow.

*/
