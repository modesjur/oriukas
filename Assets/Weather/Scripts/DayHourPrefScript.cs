﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayHourPrefScript : MonoBehaviour
{
    public int id;

    //api
    public DateTime forecastTimeUtc;
    public string conditionCode;
    public float airTemperature, cloudCover, relativeHumidity, seaLevelPressure, totalPrecipitation, windDirection, windGust, windSpeed;

    //calculated
    public DateTime forecastTimeByTimeZone;
    public float realFeel;
    public string ChanceOfRain;
    public float ChanceOfRainInt;
    public bool Night;

    //objects
    public GameObject TemperatureObj, ChanceOfRainObj, HourObj;
    public Image ConditionImg;
    
    public void CalcChanceOfRain()
    {
        if (totalPrecipitation <= 0) ChanceOfRain = "";
        else
        {
            float chance = totalPrecipitation * 100;

            if (chance > 30 && chance < 50) chance *= 0.65f;
            else if (chance >= 50 && chance < 125) chance *= 0.8f;
            else if (chance >= 125) chance = 100;

            chance = Mathf.RoundToInt(chance / 10) * 10;
            ChanceOfRain = chance + "%";
            ChanceOfRainInt = chance;
        }
    }

    public void CalcRealFeel()
    {
        realFeel = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().CalcRealFeel(airTemperature, relativeHumidity, windSpeed);
    }

    public void AllCalculations()
    {
        CalcChanceOfRain();
        CalcRealFeel();

        if (GameObject.Find("PlacePanel").GetComponent<PlaceScript>().sunriseToday != null && !GameObject.Find("PlacePanel").GetComponent<PlaceScript>().sunriseToday.Equals(""))
        {
            string HHmm = forecastTimeByTimeZone.ToString("HH:mm");
            Night = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().IsItNight(HHmm);
            AddConditionImg();
        }
        
    }
    public void AddConditionImg()
    {
        ConditionImg = transform.Find("Image").GetComponent<Image>();

        if (conditionCode.Equals("clear"))
        {
            if (!Night) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[0];
            else ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[1];
        }
        else if (conditionCode.Equals("isolated-clouds"))
        {
            if (!Night) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[2];
            else ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[3];
        }
        else if (conditionCode.Equals("scattered-clouds"))
        {
            if (!Night) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[4];
            else ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[5];
        }
        else if (conditionCode.Equals("overcast")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[6];
        else if (conditionCode.Equals("light-rain"))
        {
            if (!Night) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[7];
            else ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[8];
        }
        else if (conditionCode.Equals("moderate-rain")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[9];
        else if (conditionCode.Equals("heavy-rain")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[10];
        else if (conditionCode.Equals("sleet")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[11];
        else if (conditionCode.Equals("light-snow")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[12];
        else if (conditionCode.Equals("moderate-snow")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[13];
        else if (conditionCode.Equals("heavy-snow")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[14];
        else if (conditionCode.Equals("fog")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[15];
        else if (conditionCode.Equals("na")) ConditionImg.sprite = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().Conditions[16];
        else Debug.Log("NO DATA FOR HOURS IMG");
    }

    public void UpdateUIData()
    {
        AllCalculations();

        if (UserSettings.Instance.GetC()) //if °C
        {
            if (UserSettings.Instance.GetRoundTemp()) //Round kol kas nuo .5 
            {
                TemperatureObj.GetComponent<Text>().text = Mathf.RoundToInt(airTemperature).ToString() + "°";
            }
            else
            {
                TemperatureObj.GetComponent<Text>().text = ((Mathf.Round(airTemperature * 10)) / 10.0f).ToString() + "°";
            }
        }
        else //if °F
        {
            float f = GameObject.Find("PlacePanel").GetComponent<PlaceScript>().CelsiusToFahrenheit(airTemperature);
            if (UserSettings.Instance.GetRoundTemp()) //Round kol kas nuo .5 
            {
                TemperatureObj.GetComponent<Text>().text = Mathf.RoundToInt(f).ToString() + "°";
            }
            else
            {
                TemperatureObj.GetComponent<Text>().text = ((Mathf.Round(f * 10)) / 10.0f).ToString() + "°";
            }
        }



        if (id==0) HourObj.GetComponent<Text>().text = "Dabar";
        else HourObj.GetComponent<Text>().text = forecastTimeByTimeZone.ToString("HH");

        ChanceOfRainObj.GetComponent<Text>().text = ChanceOfRain;

        
    }

}
