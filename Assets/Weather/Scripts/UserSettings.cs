﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserSettings : MonoBehaviour
{
    private bool SaveExists = false; // PAKEIST PO TO !!!
    public GameObject SettingsPanel;
    private bool RoundTemp, SmoothHourlyTransition, C;

    public GameObject ToggleC, ToggleF, ToggleSmooth, ToggleNotSmooth, ToggleRounded, ToggleNotRounded;
    public GameObject ToggleCON, ToggleFON, ToggleSmoothON, ToggleNotSmoothON, ToggleRoundedON, ToggleNotRoundedON;

    public DateTime appLastUpdatedForecastTimeUtc, appLastMinuteUpdateTimeUtc;
    public static UserSettings Instance { get; private set; }

    public bool SettingsChanged = false;
    void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }

        QualitySettings.vSyncCount = 0; //isjungia vSync
        Application.targetFrameRate = 60;
    }

    /*
    public void FPS0()
    {
        Application.targetFrameRate = 0;
    }

    public void FPS30()
    {
        Application.targetFrameRate = 30;
    }
    public void FPS60()
    {
        Application.targetFrameRate = 60;
    }
    public void FPS120()
    {
        Application.targetFrameRate = 120;
    }
    public void FPS240()
    {
        Application.targetFrameRate = 240;
    }
    public void FPS500()
    {
        Application.targetFrameRate = 500;
    }
    public void vSyncON()
    {
        QualitySettings.vSyncCount = 1;
    }
    public void vSyncOFF()
    {
        QualitySettings.vSyncCount = 0;
    }
    */
    public void TriggerToggleC()
    {
        if(ToggleC.GetComponent<Toggle>().isOn)
        {
            ToggleF.GetComponent<Toggle>().isOn = false;
            ToggleCON.SetActive(true);
            ToggleFON.SetActive(false);
            C = true;
        }
        else
        {
            ToggleF.GetComponent<Toggle>().isOn = true;
            ToggleFON.SetActive(true);
            ToggleCON.SetActive(false);
            C = false;
        }

        SettingsChanged = true;
    }

    public void TriggerToggleF()
    {
        if (ToggleF.GetComponent<Toggle>().isOn)
        {
            ToggleC.GetComponent<Toggle>().isOn = false;
            ToggleFON.SetActive(true);
            ToggleCON.SetActive(false);
            C = false;
        }
        else
        {
            ToggleC.GetComponent<Toggle>().isOn = true;
            ToggleFON.SetActive(true);
            ToggleFON.SetActive(false);
            C = true;
        }
        SettingsChanged = true;
    }

    public void TriggerToggleSmooth()
    {
        if (ToggleSmooth.GetComponent<Toggle>().isOn)
        {
            ToggleNotSmooth.GetComponent<Toggle>().isOn = false;
            ToggleSmoothON.SetActive(true);
            ToggleNotSmoothON.SetActive(false);
            SmoothHourlyTransition = true;
        }
        else
        {
            ToggleNotSmooth.GetComponent<Toggle>().isOn = true;
            ToggleNotSmoothON.SetActive(true);
            ToggleSmoothON.SetActive(false);
            SmoothHourlyTransition = false;
        }
        SettingsChanged = true;
    }

    public void TriggerToggleNotSmooth()
    {
        if (ToggleNotSmooth.GetComponent<Toggle>().isOn)
        {
            ToggleSmooth.GetComponent<Toggle>().isOn = false;
            ToggleNotSmoothON.SetActive(true);
            ToggleSmoothON.SetActive(false);
            SmoothHourlyTransition = false;
        }
        else
        {
            ToggleSmooth.GetComponent<Toggle>().isOn = true;
            ToggleSmoothON.SetActive(true);
            ToggleNotSmoothON.SetActive(false);
            SmoothHourlyTransition = true;
        }
        SettingsChanged = true;
    }

    public void TriggerToggleRounded()
    {
        if (ToggleRounded.GetComponent<Toggle>().isOn)
        {
            ToggleNotRounded.GetComponent<Toggle>().isOn = false;
            ToggleRoundedON.SetActive(true);
            ToggleNotRoundedON.SetActive(false);
            RoundTemp = true;
        }
        else
        {
            ToggleNotRounded.GetComponent<Toggle>().isOn = true;
            ToggleNotRoundedON.SetActive(true);
            ToggleRoundedON.SetActive(false);
            RoundTemp = false;
        }
        SettingsChanged = true;
    }
    public void TriggerToggleNotRounded()
    {
        if (ToggleNotRounded.GetComponent<Toggle>().isOn)
        {
            ToggleRounded.GetComponent<Toggle>().isOn = false;
            ToggleNotRoundedON.SetActive(true);
            ToggleRoundedON.SetActive(false);
            RoundTemp = false;
        }
        else
        {
            ToggleRounded.GetComponent<Toggle>().isOn = true;
            ToggleRoundedON.SetActive(true);
            ToggleNotRoundedON.SetActive(false);
            RoundTemp = true;
        }
        SettingsChanged = true;
    }
    void Start()
    {
        //testui
        /*Text t = GameObject.Find("RezTestText").GetComponent<Text>();
        t.text = "";
        Resolution[] resolutions = Screen.resolutions;
        // Print the resolutions
        foreach (var res in resolutions)
        {
            Debug.Log(res.width + "x" + res.height + " : " + res.refreshRate);
            t.text += res.width + "x" + res.height + " : " + res.refreshRate + "\n";
        }
        Debug.Log("Screen.currentResolution.refreshRate: " + Screen.currentResolution.refreshRate);
        t.text += "current.refreshRate: " + Screen.currentResolution.refreshRate + "\n";
        */


        if (!SaveExists)
        {
            RoundTemp = true;
            SmoothHourlyTransition = true;
            C = true;
            appLastUpdatedForecastTimeUtc = System.DateTime.UtcNow.AddHours(-2); //pridedam valandas kad po to iskart update darytu API
            appLastMinuteUpdateTimeUtc = System.DateTime.UtcNow.AddMinutes(-2); //pridedam min kad po to iskart Apskaiciuotu 1h - 2h tarpa

            //save to playerprefs 
        }
        else
        {
            //Load from PlayerPrefs
        }
    }
    private void Update()
    {
        if(SettingsChanged)
        {
            SettingsChanged = false;
            GameObject.Find("PlacePanel").GetComponent<PlaceScript>().UpdateUI();
        }
    }

    public void OpenSettings()
    {
        if (SettingsPanel != null)
        {
            Animator animator = SettingsPanel.GetComponent<Animator>();
            if (animator != null)
            {
                GameObject.Find("PlacePanel").GetComponent<PlaceScript>().SettingsInfoScrollbar.value = 1;
                animator.SetBool("Open", true);
            }
        }
    }

    public void CloseSettings()
    {
        if (SettingsPanel != null)
        {
            Animator animator = SettingsPanel.GetComponent<Animator>();
            if (animator != null)
            {
                GameObject.Find("PlacePanel").GetComponent<PlaceScript>().FixScrollBarPlacePanel();
                animator.SetBool("Open", false);
            }
        }
    }

    public void SwapSettingC()
    {
        if (C) C = false;
        else C = true;

        //save to playerprefs 
    }
    public void SwapSettingRoundTemp()
    {
        if (RoundTemp) RoundTemp = false;
        else RoundTemp = true;

        //save to playerprefs 
    }
    public void SwapSettingSmoothHourlyTransition()
    {
        if (SmoothHourlyTransition) SmoothHourlyTransition = false;
        else SmoothHourlyTransition = true;

        //save to playerprefs 
    }
    public bool GetC()
    {
        return C;
    }
    public bool GetRoundTemp()
    {
        return RoundTemp;
    }
    public bool GetSmoothHourlyTransition()
    {
        return SmoothHourlyTransition;
    }
}
