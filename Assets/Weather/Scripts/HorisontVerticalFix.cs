﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

[RequireComponent(typeof(ScrollRect))]
public class HorisontVerticalFix : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    public ScrollRect OtherScrollRect;
    private ScrollRect _myScrollRect;
    private bool scrollOther;
    private bool scrollOtherHorizontally;

    void Awake()
    {
        _myScrollRect = this.GetComponent<ScrollRect>();
        scrollOtherHorizontally = _myScrollRect.vertical;
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        float horizontal = Mathf.Abs(eventData.position.x - eventData.pressPosition.x);
        float vertical = Mathf.Abs(eventData.position.y - eventData.pressPosition.y);
        if (scrollOtherHorizontally)
        {
            if (horizontal > vertical)
            {
                scrollOther = true;
                _myScrollRect.enabled = false;
                OtherScrollRect.OnBeginDrag(eventData);
            }
        }
        else if (vertical > horizontal)
        {
            scrollOther = true;
            _myScrollRect.enabled = false;
            OtherScrollRect.OnBeginDrag(eventData);
        }
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        if (scrollOther)
        {
            scrollOther = false;
            _myScrollRect.enabled = true;
            OtherScrollRect.OnEndDrag(eventData);
        }
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (scrollOther)
        {
            OtherScrollRect.OnDrag(eventData);
        }
    }
}
