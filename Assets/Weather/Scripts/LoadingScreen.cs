﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreen : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject LoadingScreenPanel;
    public bool LoadingScreenActive = true;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("PlacePanel").GetComponent<PlaceScript>().LoadChecker)
        {
            StartCoroutine(LoadingWaitting(2));
        }
    }

    IEnumerator LoadingWaitting(int sec)
    {
        yield return new WaitForSeconds(sec);
        CloseLoading();
    }

    public void OpenLoading()
    {
        if (LoadingScreenPanel != null)
        {
            Animator animator = LoadingScreenPanel.GetComponent<Animator>();
            if (animator != null)
            {
                animator.SetBool("Open", false);
                LoadingScreenActive = true;
            }
        }
    }

    public void CloseLoading()
    {
        if (LoadingScreenPanel != null)
        {
        //    if (!LoadingScreenActive)
        //    {
                Animator animator = LoadingScreenPanel.GetComponent<Animator>();
                if (animator != null)
                {
                    animator.SetBool("Open", true);
                    LoadingScreenActive = false;
                }
          //  }
        }
    }
}
