﻿using NUnit.Framework;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using UnityEngine;

public class UnitTests
{
    [Test, Description("Unit Tests/1_1 Saraso gavimas is API")]
    public void ID_1_1_1_Gauti_Sarasa()
    {
        string meteo_places = "https://api.meteo.lt/v1/places/";
			List<string[]> locations = new List<string[]>();
			bool reachable;
			try
			{
				string getLocationList = new WebClient().DownloadString(meteo_places);
				getLocationList = "{places:" + getLocationList + "}";
				JSONNode jsonNode = SimpleJSON.JSON.Parse(getLocationList);

				int index = 0;
				foreach (JSONNode o in jsonNode["places"].Children)
				{
					string[] location = new string[] { "", "", "" };

					location[0] = o["name"].ToString().Replace("\"", ""); // nuimti kabutes

					location[1] = o["code"].ToString().Replace("\"", ""); // nuimti kabutes

					location[2] = index.ToString();

					locations.Add(location);
					index++;
				}
				reachable = true;

			}
			catch (Exception e) 
			{
				reachable = false;
			}

			if(reachable)
			{
				if (locations.Any()) Assert.Pass("Sarasas gautas is API");
				else Assert.Fail("Sarasas tuscias. Nepavyko gauti saraso is API");
			}
			else Assert.Fail("Nepavyko prisijungti prie API");
    }

	[Test, Description("Unit Tests/1_2 Lokacijos prognozes gavimas is API")]
	public void ID_1_2_1_Surasti_prognoze_vilnius()
	{
		string apiRequest = "https://api.meteo.lt/v1/places/vilnius/forecasts/long-term";
		string getLocationList = "";
		bool reachable;
		try
		{
			getLocationList = new WebClient().DownloadString(apiRequest);
			reachable = true;
		}
		catch (Exception e)
		{
			reachable = false;
		}

		if (reachable)
		{
			if (!string.IsNullOrEmpty(getLocationList)) Assert.Pass("Miesto prognoze sekmingai pasiekta is API");
			else Assert.Fail("Miestas nenustatytas. Prognoze negauta");
		}
		else Assert.Fail("Nepavyko prisijungti prie API");
	}

	[Test, Description("Unit Tests/1_2 Lokacijos prognozes gavimas is API")]
	public void ID_1_2_2_Surasti_prognoze_tuscia()
	{
		string apiRequest = "https://api.meteo.lt/v1/places//forecasts/long-term";
		string getLocationList = "";
		bool reachable;
		try
		{
			getLocationList = new WebClient().DownloadString(apiRequest);
			reachable = true;
		}
		catch (Exception e)
		{
			reachable = false;
		}

		if (reachable)
		{
			if (!string.IsNullOrEmpty(getLocationList)) Assert.Fail("Surastas neegzistuojantis miestas.");
			else Assert.Pass("Neegzistuojantis miestas nenustatytas.");
		}
		else Assert.Pass("Nepavyko prisijungti prie API. Testas sekmingas");
	}

	[Test, Description("Unit Tests/1_2 Lokacijos prognozes gavimas is API")]
	public void ID_1_2_3_Surasti_prognoze_vilniusantakalnis()
	{
		string apiRequest = "https://api.meteo.lt/v1/places/vilnius-antakalnis/forecasts/long-term";
		string getLocationList = "";
		bool reachable;
		try
		{
			getLocationList = new WebClient().DownloadString(apiRequest);
			reachable = true;
		}
		catch (Exception e)
		{
			reachable = false;
		}

		if (reachable)
		{
			if (!string.IsNullOrEmpty(getLocationList)) Assert.Pass("Miesto prognoze sekmingai pasiekta is API");
			else Assert.Fail("Miestas nenustatytas. Prognoze negauta");
		}
		else Assert.Fail("Nepavyko prisijungti prie API");
	}

	[Test, Description("Unit Tests/6_3 Juntamos temperaturos apskaiciavimas")]
	public void ID_6_3_1_Suskaiciuoti_kai_visi_yra_0()
	{
		float C = 0;
		float H = 0;
		float MS = 0;
		float ats = -4;

		var mockPlaceScript = new GameObject().AddComponent<PlaceScript>();
		float calculation = mockPlaceScript.CalcRealFeel(C, H, MS);

		if (Mathf.Round(calculation).Equals(ats)) Assert.Pass("Suskaiciuota teisingai. Kai Temperatura=" + C + ", Dregme= " + H + ", Vejo Greitis=" + MS + ", Tada Juntamoji temp.=" + ats);
		else Assert.Fail("Suskaiciuota neteisingai. Teisingas ats = " + ats + " . Suskaiciuotas ats = " + calculation);
	}

	[Test, Description("Unit Tests/6_3 Juntamos temperaturos apskaiciavimas")]
	public void ID_6_3_2_Suskaiciuoti_kai_5_50_10()
	{
		float C = 5;
		float H = 50;
		float MS = 10;
		float ats = Mathf.Round(-0.4106882f);

		var mockPlaceScript = new GameObject().AddComponent<PlaceScript>();
		float calculation = Mathf.Round(mockPlaceScript.CalcRealFeel(C, H, MS));

		if (calculation.Equals(ats)) Assert.Pass("Suskaiciuota teisingai. Kai Temperatura=" + C + ", Dregme= " + H + ", Vejo Greitis=" + MS + ", Tada Juntamoji temp.=" + ats);
		else Assert.Fail("Suskaiciuota neteisingai. Teisingas ats = " + ats + " . Suskaiciuotas ats = " + calculation);
	}

	[Test, Description("Unit Tests/6_3 Juntamos temperaturos apskaiciavimas")]
	public void ID_6_3_3_Suskaiciuoti_kai_1_1_10()
	{
		float C = 1;
		float H = 1;
		float MS = 10;
		float ats = Mathf.Round(-5.708205f);

		var mockPlaceScript = new GameObject().AddComponent<PlaceScript>();
		float calculation = Mathf.Round(mockPlaceScript.CalcRealFeel(C, H, MS));

		if (calculation.Equals(ats)) Assert.Pass("Suskaiciuota teisingai. Kai Temperatura=" + C + ", Dregme= " + H + ", Vejo Greitis=" + MS + ", Tada Juntamoji temp.=" + ats);
		else Assert.Fail("Suskaiciuota neteisingai. Teisingas ats = " + ats + " . Suskaiciuotas ats = " + calculation);
	}

	public string CalcChanceOfRain(float Precipitation)
	{
		string ChanceOfRain = "-";
		if (Precipitation <= 0) ChanceOfRain = "0%";
		else
		{
			float chance = Precipitation * 100;

			if (chance > 30 && chance < 50) chance *= 0.65f;
			else if (chance >= 50 && chance < 125) chance *= 0.8f;
			else if (chance >= 125) chance = 100;

			chance = Mathf.RoundToInt(chance / 10) * 10;
			ChanceOfRain = chance + "%";
		}
		return ChanceOfRain;
	}

	[Test, Description("Unit Tests/7_1 Lietaus tikimybes apskaiciavimas")]
	public void ID_7_1_1_Suskaiciuoti_kai_krituliu_kiekis_0()
	{
		float P = 0;
		string ats = "0%";

		if (CalcChanceOfRain(P).Equals(ats)) Assert.Pass("Suskaiciuota teisingai. Kai Lietaus kiekis=" + P + ", Tada Lietaus tikimybe=" + ats);
		else Assert.Fail("Suskaiciuota neteisingai");
	}

	[Test, Description("Unit Tests/7_1 Lietaus tikimybes apskaiciavimas")]
	public void ID_7_1_2_Suskaiciuoti_kai_krituliu_kiekis_1_25()
	{
		float P = 1.25f;
		string ats = "100%";

		if (CalcChanceOfRain(P).Equals(ats)) Assert.Pass("Suskaiciuota teisingai. Kai Lietaus kiekis=" + P + ", Tada Lietaus tikimybe=" + ats);
		else Assert.Fail("Suskaiciuota neteisingai");
	}

	[Test, Description("Unit Tests/7_1 Lietaus tikimybes apskaiciavimas")]
	public void ID_7_1_3_Suskaiciuoti_kai_krituliu_kiekis_0_8()
	{
		float P = 0.8f;
		string ats = "60%";

		if (CalcChanceOfRain(P).Equals(ats)) Assert.Pass("Suskaiciuota teisingai. Kai Lietaus kiekis=" + P + ", Tada Lietaus tikimybe=" + ats);
		else Assert.Fail("Suskaiciuota neteisingai");
	}

	[Test, Description("Unit Tests/8_1 Suskaiciuoti dienos ir nakties temperatūros ekstremumus")]
	public void ID_8_1_1_Suskaiciuoti_dienos_didziausia_temp_kai_1_4_5_3_0()
	{
		List<float> DayTemp = new List<float>();
		DayTemp.Add(1);
		DayTemp.Add(4);
		DayTemp.Add(5);
		DayTemp.Add(3);
		DayTemp.Add(0);

		float ats = 5;

		var mockWeekDay = new GameObject().AddComponent<WeekDayScript>();

		if (mockWeekDay.maxValueInList(DayTemp).Equals(ats)) Assert.Pass("Suskaiciuota teisingai. Didziausia reiksme =" + ats);
		else Assert.Fail("Suskaiciuota neteisingai");
	}

	[Test, Description("Unit Tests/8_1 Suskaiciuoti dienos ir nakties temperatūros ekstremumus")]
	public void ID_8_1_2_Suskaiciuoti_dienos_didziausia_temp_kai_5_17_15_18_13()
	{
		List<float> DayTemp = new List<float>();
		DayTemp.Add(5);
		DayTemp.Add(17);
		DayTemp.Add(15);
		DayTemp.Add(18);
		DayTemp.Add(13);

		float ats = 18;

		var mockWeekDay = new GameObject().AddComponent<WeekDayScript>();

		if (mockWeekDay.maxValueInList(DayTemp).Equals(ats)) Assert.Pass("Suskaiciuota teisingai. Didziausia reiksme =" + ats);
		else Assert.Fail("Suskaiciuota neteisingai");
	}

	[Test, Description("Unit Tests/8_1 Suskaiciuoti dienos ir nakties temperatūros ekstremumus")]
	public void ID_8_1_3_Suskaiciuoti_nakties_maziausia_temp_kai_0_m4_m1_3_0()
	{
		List<float> NightTemp = new List<float>();
		NightTemp.Add(0);
		NightTemp.Add(-4);
		NightTemp.Add(-1);
		NightTemp.Add(3);
		NightTemp.Add(0);

		float ats = -4;

		var mockWeekDay = new GameObject().AddComponent<WeekDayScript>();

		if (mockWeekDay.minValueInList(NightTemp).Equals(ats)) Assert.Pass("Suskaiciuota teisingai. Maziausia reiksme =" + ats);
		else Assert.Fail("Suskaiciuota neteisingai");
	}

	[Test, Description("Unit Tests/8_1 Suskaiciuoti dienos ir nakties temperatūros ekstremumus")]
	public void ID_8_1_4_Suskaiciuoti_nakties_maziausia_temp_kai_2_m1_m2_2_1()
	{
		List<float> NightTemp = new List<float>();
		NightTemp.Add(2);
		NightTemp.Add(-1);
		NightTemp.Add(-2);
		NightTemp.Add(2);
		NightTemp.Add(1);

		float ats = -2;

		var mockWeekDay = new GameObject().AddComponent<WeekDayScript>();

		if (mockWeekDay.minValueInList(NightTemp).Equals(ats)) Assert.Pass("Suskaiciuota teisingai. Maziausia reiksme =" + ats);
		else Assert.Fail("Suskaiciuota neteisingai");
	}

	[Test, Description("Unit Tests/8_2 Apskaiciuoti tikimybe kitos paros oro tipui")]
	public void ID_8_2_1_Apskaiciuoti_tikimybe_kitos_paros_oro_tipui_kai_giedraX3_debesuotaX2()
	{
		List<string> Types = new List<string>();
		Types.Add("clear");
		Types.Add("clear");
		Types.Add("overcast");
		Types.Add("overcast");
		Types.Add("clear");

		string ats = "clear";

		var mockWeekDay = new GameObject().AddComponent<WeekDayScript>();

		if (mockWeekDay.MostCommonStringInList(Types).Equals(ats)) Assert.Pass("Atrinkta teisingai. paros reiksme =" + ats);
		else Assert.Fail("Atrinkta neteisingai");
	}

	[Test, Description("Unit Tests/8_2 Apskaiciuoti tikimybe kitos paros oro tipui")]
	public void ID_8_2_2_Apskaiciuoti_tikimybe_kitos_paros_oro_tipui_kai_debesuotaX3_slapdribaX1_giedraX1()
	{
		List<string> Types = new List<string>();
		Types.Add("overcast");
		Types.Add("sleet");
		Types.Add("overcast");
		Types.Add("overcast");
		Types.Add("clear");

		string ats = "overcast";

		var mockWeekDay = new GameObject().AddComponent<WeekDayScript>();

		if (mockWeekDay.MostCommonStringInList(Types).Equals(ats)) Assert.Pass("Atrinkta teisingai. paros reiksme =" + ats);
		else Assert.Fail("Atrinkta neteisingai");
	}

	[Test, Description("Unit Tests/14_1 LT RU LV EE ir BY rasmenu atpazinimas")]
	public void ID_14_1_1_Isvalyti_ivesti_kai_LT_raides()
	{
		string input = "žąsis";
		string ats = "zasis";

		var mockPlaceScript = new GameObject().AddComponent<PlaceScript>();

		if (mockPlaceScript.CleanPlaceName(input).Equals(ats)) Assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
		else Assert.Fail("Konvertuota neteisingai");
	}

	[Test, Description("Unit Tests/14_1 LT RU LV EE ir BY rasmenu atpazinimas")]
	public void ID_14_1_2_Isvalyti_ivesti_kai_RU_raides()
	{
		string input = "вгш";
		string ats = "vgs";

		var mockPlaceScript = new GameObject().AddComponent<PlaceScript>();

		if (mockPlaceScript.CleanPlaceName(input).Equals(ats)) Assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
		else Assert.Fail("Konvertuota neteisingai");
	}

	[Test, Description("Unit Tests/18_1 Temperaturu konvertavimas is C i F ir atvirksciai")]
	public void ID_18_1_1_Konvertavimas_is_C_i_F_kai_C_30()
	{
		float C = 30;
		float ats = 86;

		var mockPlaceScript = new GameObject().AddComponent<PlaceScript>();

		if (mockPlaceScript.CelsiusToFahrenheit(C).Equals(ats)) Assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
		else Assert.Fail("Konvertuota neteisingai");
	}

	[Test, Description("Unit Tests/18_1 Temperaturu konvertavimas is C i F ir atvirksciai")]
	public void ID_18_1_2_Konvertavimas_is_C_i_F_kai_C_0()
	{
		float C = 0;
		float ats = 32;

		var mockPlaceScript = new GameObject().AddComponent<PlaceScript>();

		if (mockPlaceScript.CelsiusToFahrenheit(C).Equals(ats)) Assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
		else Assert.Fail("Konvertuota neteisingai");
	}

	[Test, Description("Unit Tests/18_1 Temperaturu konvertavimas is C i F ir atvirksciai")]
	public void ID_18_1_3_Konvertavimas_is_F_i_C_kai_F_50()
	{
		float F = 50;
		float ats = 10;

		var mockPlaceScript = new GameObject().AddComponent<PlaceScript>();

		if (mockPlaceScript.FahrenheitToCelsius(F).Equals(ats)) Assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
		else Assert.Fail("Konvertuota neteisingai");
	}

	[Test, Description("Unit Tests/18_1 Temperaturu konvertavimas is C i F ir atvirksciai")]
	public void ID_18_1_4_Konvertavimas_is_F_i_C_kai_F_0()
	{
		float F = -4;
		float ats = -20;

		var mockPlaceScript = new GameObject().AddComponent<PlaceScript>();

		if (mockPlaceScript.FahrenheitToCelsius(F).Equals(ats)) Assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
		else Assert.Fail("Konvertuota neteisingai");
	}

	[Test, Description("Unit Tests/18_2 Proporcinis duomenu apskaiciavimas dvieju skaiciu intervale pagal dabartine valanda ir minutes")]
	public void ID_18_2_1_Apskaiciavimas_4_6_kai_min_30()
	{
		float a = 4;
		float b = 6;
		int min = 30;
		float ats = 5;

		if (CalcSmoothHourlyTransition(a, b, min).Equals(ats)) Assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
		else Assert.Fail("Konvertuota neteisingai");
	}

	[Test, Description("Unit Tests/18_2 Proporcinis duomenu apskaiciavimas dvieju skaiciu intervale pagal dabartine valanda ir minutes")]
	public void ID_18_2_1_Apskaiciavimas_10_20_kai_min_15()
	{
		float a = 10;
		float b = 30;
		int min = 15;
		float ats = 15;

		if (CalcSmoothHourlyTransition(a, b, min).Equals(ats)) Assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
		else Assert.Fail("Konvertuota neteisingai");
	}

	//Konvertavimo funkcija gauti tarpini skaiciu x/60 proporcija funkcija
	public float CalcSmoothHourlyTransition(float x, float y, int min)
	{
		float Transition;
		if (min < 0) min = 0;
		else if (min > 60) min = 60;

		Transition = x - ((x - y) * min / 60);

		return Transition;
	}
}
