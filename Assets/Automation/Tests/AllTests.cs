﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.Net;

namespace TrilleonAutomation {

	[AutomationClass]
	public class AllTests : MonoBehaviour {


		[Automation("Unit Tests/1_1 Saraso gavimas is API")]
		public IEnumerator ID_1_1_1_Gauti_Sarasa()
		{
			string meteo_places = "https://api.meteo.lt/v1/places/";
			List<string[]> locations = new List<string[]>();
			bool reachable;
			try
			{
				string getLocationList = new WebClient().DownloadString(meteo_places);
				getLocationList = "{places:" + getLocationList + "}";
				JSONNode jsonNode = SimpleJSON.JSON.Parse(getLocationList);

				int index = 0;
				foreach (JSONNode o in jsonNode["places"].Children)
				{
					string[] location = new string[] { "", "", "" };

					location[0] = o["name"].ToString().Replace("\"", ""); // nuimti kabutes

					location[1] = o["code"].ToString().Replace("\"", ""); // nuimti kabutes

					location[2] = index.ToString();

					locations.Add(location);
					index++;
				}
				reachable = true;

			}
			catch (Exception e) 
			{
				reachable = false;
			}

			if(reachable)
			{
				if (locations.Count > 0) yield return Q.assert.Pass("Sarasas gautas is API");
				else yield return Q.assert.Fail("Sarasas tuscias. Nepavyko gauti saraso is API");
			}
			else yield return Q.assert.Fail("Nepavyko prisijungti prie API");

		}

		[Automation("Unit Tests/1_2 Lokacijos prognozes gavimas is API")]
		public IEnumerator ID_1_2_1_Surasti_prognoze_vilnius()
		{
			string apiRequest = "https://api.meteo.lt/v1/places/vilnius/forecasts/long-term";
			string getLocationList = "";
			bool reachable;
			try
			{
				getLocationList = new WebClient().DownloadString(apiRequest);
				reachable = true;
			}
			catch (Exception e)
			{
				reachable = false;
			}

			if (reachable)
			{
				if (!getLocationList.Equals("")) yield return Q.assert.Pass("Miesto prognoze sekmingai pasiekta is API");
				else yield return Q.assert.Fail("Miestas nenustatytas. Prognoze negauta");
			}
			else yield return Q.assert.Fail("Nepavyko prisijungti prie API");
		}

		[Automation("Unit Tests/1_2 Lokacijos prognozes gavimas is API")]
		public IEnumerator ID_1_2_2_Surasti_prognoze_tuscia()
		{
			string apiRequest = "https://api.meteo.lt/v1/places//forecasts/long-term";
			string getLocationList = "";
			bool reachable;
			try
			{
				getLocationList = new WebClient().DownloadString(apiRequest);
				reachable = true;
			}
			catch (Exception e)
			{
				reachable = false;
			}

			if (reachable)
			{
				if (!getLocationList.Equals("")) yield return Q.assert.Fail("Surastas neegzistuojantis miestas.");
				else yield return Q.assert.Pass("Neegzistuojantis miestas nenustatytas.");
			}
			else yield return Q.assert.Pass("Nepavyko prisijungti prie API. Testas sekmingas");
		}

		[Automation("Unit Tests/1_2 Lokacijos prognozes gavimas is API")]
		public IEnumerator ID_1_2_3_Surasti_prognoze_vilniusantakalnis()
		{
			string apiRequest = "https://api.meteo.lt/v1/places/vilnius-antakalnis/forecasts/long-term";
			string getLocationList = "";
			bool reachable;
			try
			{
				getLocationList = new WebClient().DownloadString(apiRequest);
				reachable = true;
			}
			catch (Exception e)
			{
				reachable = false;
			}

			if (reachable)
			{
				if (!getLocationList.Equals("")) yield return Q.assert.Pass("Miesto prognoze sekmingai pasiekta is API");
				else yield return Q.assert.Fail("Miestas nenustatytas. Prognoze negauta");
			}
			else yield return Q.assert.Fail("Nepavyko prisijungti prie API");
		}

		[Automation("Unit Tests/6_3 Juntamos temperaturos apskaiciavimas")]
		public IEnumerator ID_6_3_1_Suskaiciuoti_kai_visi_yra_0()
		{
			float C = 0;
			float H = 0;
			float MS = 0;
			float ats = -4;

			float calculation = CalcRealFeel(C, H, MS);

			if (Mathf.Round(calculation) == ats) yield return Q.assert.Pass("Suskaiciuota teisingai. Kai Temperatura=" + C + ", Dregme= " + H + ", Vejo Greitis=" + MS + ", Tada Juntamoji temp.=" + ats);
			else yield return Q.assert.Fail("Suskaiciuota neteisingai");
		}

		[Automation("Unit Tests/6_3 Juntamos temperaturos apskaiciavimas")]
		public IEnumerator ID_6_3_2_Suskaiciuoti_kai_5_50_10()
		{
			float C = 5;
			float H = 50;
			float MS = 10;
			float ats = -3;

			float calculation = CalcRealFeel(C, H, MS);

			if (Mathf.Round(calculation) == ats) yield return Q.assert.Pass("Suskaiciuota teisingai. Kai Temperatura=" + C + ", Dregme= " + H + ", Vejo Greitis=" + MS + ", Tada Juntamoji temp.=" + ats);
			else yield return Q.assert.Fail("Suskaiciuota neteisingai");
		}

		[Automation("Unit Tests/6_3 Juntamos temperaturos apskaiciavimas")]
		public IEnumerator ID_6_3_3_Suskaiciuoti_kai_1_1_10()
		{
			float C = 1;
			float H = 1;
			float MS = 10;
			float ats = -9;

			float calculation = CalcRealFeel(C, H, MS);

			if (Mathf.Round(calculation) == ats) yield return Q.assert.Pass("Suskaiciuota teisingai. Kai Temperatura=" + C + ", Dregme= " + H + ", Vejo Greitis=" + MS + ", Tada Juntamoji temp.=" + ats);
			else yield return Q.assert.Fail("Suskaiciuota neteisingai");
		}

		//Juntamos temperaturos apskaiciavimo formules funkcija
		public float CalcRealFeel(float Temp_C, float Humidity_Proc, float WindSpeed_MS) // temp C, Humidity % and WindSpeed MS
		{
			Temp_C = (Temp_C * 9 / 5) + 32;  // is C i F konvertuojam
			WindSpeed_MS = WindSpeed_MS * 3.6f * 1.609344f; // is MS i KMH i MPH
			float RealFeel;

			//first WindChill
			if (Temp_C <= 50 && WindSpeed_MS >= 3)
			{
				RealFeel = 35.74f + (0.6215f * Temp_C) - 35.75f * Mathf.Pow(WindSpeed_MS, 0.16f) + ((0.4275f * Temp_C) * Mathf.Pow(WindSpeed_MS, 0.16f));
			}
			else RealFeel = Temp_C;

			//Tikrint su Heat Index        
			if (RealFeel == Temp_C)
			{
				RealFeel = 0.5f * (Temp_C + 61.0f + ((Temp_C - 68.0f) * 1.2f) + (Humidity_Proc * 0.094f));

				if (RealFeel >= 80)
				{
					RealFeel = -42.379f + 2.04901523f * Temp_C + 10.14333127f * Humidity_Proc - .22475541f * Temp_C * Humidity_Proc - .00683783f * Temp_C * Temp_C
						- .05481717f * Humidity_Proc * Humidity_Proc + .00122874f * Temp_C * Temp_C * Humidity_Proc + .00085282f * Temp_C * Humidity_Proc * Humidity_Proc
						- .00000199f * Temp_C * Temp_C * Humidity_Proc * Humidity_Proc;

					if (Humidity_Proc < 13 && Temp_C >= 80 && Temp_C <= 112)
					{
						RealFeel = RealFeel - ((13 - Humidity_Proc) / 4) * Mathf.Sqrt((17 - Mathf.Abs(Temp_C - 95.0f)) / 17);

						if (Humidity_Proc > 85 && Temp_C >= 80 && Temp_C <= 87)
						{
							RealFeel = RealFeel + ((Humidity_Proc - 85) / 10) * ((87 - Temp_C) / 5);
						}
					}
				}
			}

			RealFeel = (RealFeel - 32) * 5 / 9; // F to C
			return RealFeel;
		}

		[Automation("Unit Tests/7_1 Lietaus tikimybes apskaiciavimas")]
		public IEnumerator ID_7_1_1_Suskaiciuoti_kai_krituliu_kiekis_0()
		{
			float P = 0;
			string ats = "0%";

			if (CalcChanceOfRain(P).Equals(ats)) yield return Q.assert.Pass("Suskaiciuota teisingai. Kai Lietaus kiekis=" + P + ", Tada Lietaus tikimybe=" + ats);
			else yield return Q.assert.Fail("Suskaiciuota neteisingai");
		}

		[Automation("Unit Tests/7_1 Lietaus tikimybes apskaiciavimas")]
		public IEnumerator ID_7_1_2_Suskaiciuoti_kai_krituliu_kiekis_1_25()
		{
			float P = 1.25f;
			string ats = "100%";

			if (CalcChanceOfRain(P).Equals(ats)) yield return Q.assert.Pass("Suskaiciuota teisingai. Kai Lietaus kiekis=" + P + ", Tada Lietaus tikimybe=" + ats);
			else yield return Q.assert.Fail("Suskaiciuota neteisingai");
		}

		[Automation("Unit Tests/7_1 Lietaus tikimybes apskaiciavimas")]
		public IEnumerator ID_7_1_3_Suskaiciuoti_kai_krituliu_kiekis_0_8()
		{
			float P = 0.8f;
			string ats = "60%";

			if (CalcChanceOfRain(P).Equals(ats)) yield return Q.assert.Pass("Suskaiciuota teisingai. Kai Lietaus kiekis=" + P + ", Tada Lietaus tikimybe=" + ats);
			else yield return Q.assert.Fail("Suskaiciuota neteisingai");
		}

		//Lietaus tikimybes apskaiciavimo formules funkcija
		public string CalcChanceOfRain(float Precipitation)
		{
			string ChanceOfRain = "-";
			if (Precipitation <= 0) ChanceOfRain = "0%";
			else
			{
				float chance = Precipitation * 100;

				if (chance > 30 && chance < 50) chance *= 0.65f;
				else if (chance >= 50 && chance < 125) chance *= 0.8f;
				else if (chance >= 125) chance = 100;

				chance = Mathf.RoundToInt(chance / 10) * 10;
				ChanceOfRain = chance + "%";
			}
			return ChanceOfRain;
		}

		[Automation("Unit Tests/8_1 Suskaiciuoti dienos ir nakties temperatūros ekstremumus")]
		public IEnumerator ID_8_1_1_Suskaiciuoti_dienos_didziausia_temp_kai_1_4_5_3_0()
		{
			List<float> DayTemp = new List<float>();
			DayTemp.Add(1);
			DayTemp.Add(4);
			DayTemp.Add(5);
			DayTemp.Add(3);
			DayTemp.Add(0);

			float ats = 5;

			if (maxValueInList(DayTemp)==ats) yield return Q.assert.Pass("Suskaiciuota teisingai. Didziausia reiksme =" + ats);
			else yield return Q.assert.Fail("Suskaiciuota neteisingai");
		}

		[Automation("Unit Tests/8_1 Suskaiciuoti dienos ir nakties temperatūros ekstremumus")]
		public IEnumerator ID_8_1_2_Suskaiciuoti_dienos_didziausia_temp_kai_5_17_15_18_13()
		{
			List<float> DayTemp = new List<float>();
			DayTemp.Add(5);
			DayTemp.Add(17);
			DayTemp.Add(15);
			DayTemp.Add(18);
			DayTemp.Add(13);

			float ats = 18;

			if (maxValueInList(DayTemp) == ats) yield return Q.assert.Pass("Suskaiciuota teisingai. Didziausia reiksme =" + ats);
			else yield return Q.assert.Fail("Suskaiciuota neteisingai");
		}
		//Dienos ekstremumo apskaiciavimo formules funkcija
		public float maxValueInList(List<float> list)
		{
			float max = -999;
			foreach (float t in list)
			{
				if (t > max) max = t;
			}
			return max;
		}

		[Automation("Unit Tests/8_1 Suskaiciuoti dienos ir nakties temperatūros ekstremumus")]
		public IEnumerator ID_8_1_3_Suskaiciuoti_nakties_maziausia_temp_kai_0_m4_m1_3_0()
		{
			List<float> NightTemp = new List<float>();
			NightTemp.Add(0);
			NightTemp.Add(-4);
			NightTemp.Add(-1);
			NightTemp.Add(3);
			NightTemp.Add(0);

			float ats = -4;

			if (minValueInList(NightTemp) == ats) yield return Q.assert.Pass("Suskaiciuota teisingai. Maziausia reiksme =" + ats);
			else yield return Q.assert.Fail("Suskaiciuota neteisingai");
		}

		[Automation("Unit Tests/8_1 Suskaiciuoti dienos ir nakties temperatūros ekstremumus")]
		public IEnumerator ID_8_1_4_Suskaiciuoti_nakties_maziausia_temp_kai_2_m1_m2_2_1()
		{
			List<float> NightTemp = new List<float>();
			NightTemp.Add(2);
			NightTemp.Add(-1);
			NightTemp.Add(-2);
			NightTemp.Add(2);
			NightTemp.Add(1);

			float ats = -2;

			if (minValueInList(NightTemp) == ats) yield return Q.assert.Pass("Suskaiciuota teisingai. Maziausia reiksme =" + ats);
			else yield return Q.assert.Fail("Suskaiciuota neteisingai");
		}

		//Nakties ekstremumo apskaiciavimo formules funkcija
		public float minValueInList(List<float> list)
		{
			float min = 999;
			foreach (float t in list)
			{
				if (t < min) min = t;
			}
			return min;
		}

		[Automation("Unit Tests/8_2 Apskaiciuoti tikimybe kitos paros oro tipui")]
		public IEnumerator ID_8_2_1_Apskaiciuoti_tikimybe_kitos_paros_oro_tipui_kai_giedraX3_debesuotaX2()
		{
			List<string> Types = new List<string>();
			Types.Add("clear");
			Types.Add("clear");
			Types.Add("overcast");
			Types.Add("overcast");
			Types.Add("clear");

			string ats = "clear";

			if (MostCommonStringInList(Types).Equals(ats)) yield return Q.assert.Pass("Atrinkta teisingai. paros reiksme =" + ats);
			else yield return Q.assert.Fail("Atrinkta neteisingai");
		}

		[Automation("Unit Tests/8_2 Apskaiciuoti tikimybe kitos paros oro tipui")]
		public IEnumerator ID_8_2_2_Apskaiciuoti_tikimybe_kitos_paros_oro_tipui_kai_debesuotaX3_slapdribaX1_giedraX1()
		{
			List<string> Types = new List<string>();
			Types.Add("overcast");
			Types.Add("sleet");
			Types.Add("overcast");
			Types.Add("overcast");
			Types.Add("clear");

			string ats = "overcast";

			if (MostCommonStringInList(Types).Equals(ats)) yield return Q.assert.Pass("Atrinkta teisingai. paros reiksme =" + ats);
			else yield return Q.assert.Fail("Atrinkta neteisingai");
		}

		//Paros oro tipui surasti taikoma formules funkcija
		public string MostCommonStringInList(List<string> list)
		{
			string Most = "";
			int MostInt = 0;
			Dictionary<string, int> shoppingDictionary = new Dictionary<string, int>();
			foreach (string item in list)
			{
				if (!shoppingDictionary.ContainsKey(item))
				{
					shoppingDictionary.Add(item, 1);
				}
				else
				{
					int count = 0;
					shoppingDictionary.TryGetValue(item, out count);
					shoppingDictionary.Remove(item);
					shoppingDictionary.Add(item, count + 1);
				}
			}

			foreach (KeyValuePair<string, int> entry in shoppingDictionary)
			{
				if (MostInt < entry.Value)
				{
					MostInt = entry.Value;
					Most = entry.Key;
				}
			}

			return Most;
		}

		[Automation("Unit Tests/14_1 LT RU LV EE ir BY rasmenu atpazinimas")]
		public IEnumerator ID_14_1_1_Isvalyti_ivesti_kai_LT_raides()
		{
			string input = "žąsis";
			string ats = "zasis";

			if (CleanPlaceName(input).Equals(ats)) yield return Q.assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
			else yield return Q.assert.Fail("Konvertuota neteisingai");
		}

		[Automation("Unit Tests/14_1 LT RU LV EE ir BY rasmenu atpazinimas")]
		public IEnumerator ID_14_1_2_Isvalyti_ivesti_kai_RU_raides()
		{
			string input = "вгш";
			string ats = "vgs";

			if (CleanPlaceName(input).Equals(ats)) yield return Q.assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
			else yield return Q.assert.Fail("Konvertuota neteisingai");
		}

		//Simboliu is kitu kalbu i anglu kalba konvertavimo funkcija
		string CleanPlaceName(string name)
		{
			string newName = name;

			//Fixing Typed data
			newName = newName.ToLower();
			//LT raides
			newName = newName.Replace("ą", "a").Replace("č", "c").Replace("ę", "e").Replace("ė", "e")
				.Replace("į", "i").Replace("š", "s").Replace("ų", "u").Replace("ū", "u").Replace("ž", "z");
			//LV raides
			newName = newName.Replace("ā", "a").Replace("ē", "e").Replace("ģ", "g")
			.Replace("ī", "i").Replace("ķ", "k").Replace("ļ", "l").Replace("ņ", "n");
			//BY- RU raides
			newName = newName.Replace("б", "b").Replace("в", "v").Replace("г", "g").Replace("д", "d")
			.Replace("ё", "o").Replace("ж", "s").Replace("з", "z").Replace("й", "y").Replace("к", "k").Replace("л", "l")
			.Replace("м", "m").Replace("н", "n").Replace("п", "p").Replace("т", "t").Replace("ў", "w").Replace("ф", "f")
			.Replace("ц", "c").Replace("ч", "c").Replace("ш", "s").Replace("ы", "y").Replace("ь", "").Replace("э", "e")
			.Replace("ю", "ju").Replace("я", "ja");
			//PL raides
			newName = newName.Replace("ł", "w").Replace("ń", "n").Replace("ó", "u").Replace("ś", "s")
			.Replace("ź", "z").Replace("ż", "z");
			//EE raides
			newName = newName.Replace("ä", "a").Replace("ö", "o").Replace("õ", "o").Replace("ü", "u");

			return newName;
		}

		[Automation("Unit Tests/18_1 Temperaturu konvertavimas is C i F ir atvirksciai")]
		public IEnumerator ID_18_1_1_Konvertavimas_is_C_i_F_kai_C_30()
		{
			float C = 30;
			float ats = 86;

			if (CelsiusToFahrenheit(C)==ats) yield return Q.assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
			else yield return Q.assert.Fail("Konvertuota neteisingai");
		}

		[Automation("Unit Tests/18_1 Temperaturu konvertavimas is C i F ir atvirksciai")]
		public IEnumerator ID_18_1_2_Konvertavimas_is_C_i_F_kai_C_0()
		{
			float C = 0;
			float ats = 32;

			if (CelsiusToFahrenheit(C) == ats) yield return Q.assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
			else yield return Q.assert.Fail("Konvertuota neteisingai");
		}

		//Konvertavimo funkcija is Celsius i Fahrenheit
		public float CelsiusToFahrenheit(float C)
		{
			return (C * 9 / 5) + 32;
		}

		[Automation("Unit Tests/18_1 Temperaturu konvertavimas is C i F ir atvirksciai")]
		public IEnumerator ID_18_1_3_Konvertavimas_is_F_i_C_kai_F_50()
		{
			float F = 50;
			float ats = 10;

			if (FahrenheitToCelsius(F) == ats) yield return Q.assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
			else yield return Q.assert.Fail("Konvertuota neteisingai");
		}

		[Automation("Unit Tests/18_1 Temperaturu konvertavimas is C i F ir atvirksciai")]
		public IEnumerator ID_18_1_4_Konvertavimas_is_F_i_C_kai_F_0()
		{
			float F = -4;
			float ats = -20;

			if (FahrenheitToCelsius(F) == ats) yield return Q.assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
			else yield return Q.assert.Fail("Konvertuota neteisingai");
		}

		//Konvertavimo funkcija is Fahrenheit i Celsius
		public float FahrenheitToCelsius(float F)
		{
			return (F - 32) * 5 / 9;
		}

		[Automation("Unit Tests/18_2 Proporcinis duomenu apskaiciavimas dvieju skaiciu intervale pagal dabartine valanda ir minutes")]
		public IEnumerator ID_18_2_1_Apskaiciavimas_4_6_kai_min_30()
		{
			float a = 4;
			float b = 6;
			int min = 30;
			float ats = 5;

			if (CalcSmoothHourlyTransition(a, b, min) == ats) yield return Q.assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
			else yield return Q.assert.Fail("Konvertuota neteisingai");
		}

		[Automation("Unit Tests/18_2 Proporcinis duomenu apskaiciavimas dvieju skaiciu intervale pagal dabartine valanda ir minutes")]
		public IEnumerator ID_18_2_1_Apskaiciavimas_10_20_kai_min_15()
		{
			float a = 10;
			float b = 30;
			int min = 15;
			float ats = 15;

			if (CalcSmoothHourlyTransition(a, b, min) == ats) yield return Q.assert.Pass("Konvertuota teisingai. Reiksme =" + ats);
			else yield return Q.assert.Fail("Konvertuota neteisingai");
		}

		//Konvertavimo funkcija gauti tarpini skaiciu x/60 proporcija funkcija
		public float CalcSmoothHourlyTransition(float x, float y, int min)
		{
			float Transition;
			if (min < 0) min = 0;
			else if (min > 60) min = 60;

			Transition = x - ((x - y) * min / 60);

			return Transition;
		}

		[Automation("Iterative Tests/3_1 Perejimas tarp langu")]

		public IEnumerator ID_3_1_1_Perejimas_i_paieskos_langa_ir_atgal()
		{
			GameObject parentObject = null;
			parentObject = Q.driver.Find(By.Name, "Canvas", false);
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "OpenSearchButon", false), "Click object with name OpenSearchButon"));
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "CloseSearchButon", false), "Click object with name CloseSearchButon"));
		}

		[Automation("Iterative Tests/3_1 Perejimas tarp langu")]
		public IEnumerator ID_3_1_2_Perejimas_i_nustatymu_langa_ir_atgal()
		{
			GameObject parentObject = null;
			parentObject = Q.driver.Find(By.Name, "Canvas", false);
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "OpenSettingsButon", false), "Click object with name OpenSettingsButon"));
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "BackButton", false), "Click object with name BackButton"));
		}

		[Automation("Iterative Tests/12_1 Simboliu atpazinimas saraso atnaujinimas su kiekvienu naujai ivestu simboliu")]
		public IEnumerator ID_12_1_1_Paieskoje_suvedus_pavadinima_su_LT_raidemis_Siauliai()
		{
			GameObject parentObject = null;
			parentObject = Q.driver.Find(By.Name, "Canvas", false);
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "OpenSearchButon", false), "Click object with name OpenSearchButon"));
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "InputFieldActivationButton", false), "Click object with name InputFieldActivationButton"));
			GameObject middleLevelObject = null;
			middleLevelObject = Q.driver.FindIn(parentObject, By.Name, "Autocomplete", false);
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(middleLevelObject, By.Name, "Šiauliai", false), "Click object with name Šiauliai"));
		}

		[Automation("Iterative Tests/12_1 Simboliu atpazinimas saraso atnaujinimas su kiekvienu naujai ivestu simboliu")]
		public IEnumerator ID_12_1_2_Paieskoje_suvedus_pavadinima_su_LT_raidemis_Mazeikiai()
		{
			GameObject parentObject = null;
			parentObject = Q.driver.Find(By.Name, "Canvas", false);
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "OpenSearchButon", false), "Click object with name OpenSearchButon"));
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "InputFieldActivationButton", false), "Click object with name InputFieldActivationButton"));
			GameObject middleLevelObject = null;
			middleLevelObject = Q.driver.FindIn(parentObject, By.Name, "Autocomplete", false);
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(middleLevelObject, By.Name, "Mažeikiai", false), "Click object with name Mažeikiai"));
		}

		[Automation("Iterative Tests/13_1 Paieska pagal nurodyta vietove")]
		public IEnumerator ID_13_1_1_Suvesti_vietove_Vilnius()
		{
			GameObject parentObject = null;
			parentObject = Q.driver.Find(By.Name, "Canvas", false);
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "OpenSearchButon", false), "Click object with name OpenSearchButon"));
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "InputFieldActivationButton", false), "Click object with name InputFieldActivationButton"));
			GameObject middleLevelObject = null;
			middleLevelObject = Q.driver.FindIn(parentObject, By.Name, "Autocomplete", false);
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(middleLevelObject, By.Name, "Vilnius", false), "Click object with name Vilnius"));
		}

		[Automation("Iterative Tests/13_1 Paieska pagal nurodyta vietove")]
		public IEnumerator ID_13_1_2_Nerasti_vietoves_suvedus_aaaaa()
		{
			GameObject parentObject = null;
			parentObject = Q.driver.Find(By.Name, "Canvas", false);
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "OpenSearchButon", false), "Click object with name OpenSearchButon"));
			yield return StartCoroutine(Q.driver.Click(Q.driver.FindIn(parentObject, By.Name, "InputFieldActivationButton", false), "Click object with name InputFieldActivationButton"));
		}


	}

}